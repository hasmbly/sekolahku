<?php

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');

Route::get('/home', 'HomeController@index')->name('home');

/** Just Contoh, untuk route group middleware dengan multiple role
 *
 * Route::group(['middleware' => ['role:ROLE_SUPERADMIN,ROLE_ADMIN,ROLE_SUPER,ROLE_USERS']], function () {
 * Route::resource('user', 'UserController', ['except' => ['show']]);
 * });
 *
 */

Route::group(['middleware' => 'auth'], function () {
    Route::group(['middleware' => ['role:ROLE_SUPERADMIN']], function () {
        Route::resource('user', 'UserController', ['except' => ['show']]);
    });

	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'ProfileController@update']);
    Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'ProfileController@password']);

    //Cek Pembayaran
    Route::resource('pembayaran', 'PembayaranController');
    Route::post('search_pembayaran', 'PembayaranController@SearchData');

    //Siswa
    Route::resource('master/siswa', 'Master\SiswaController');
    Route::post('master/search_siswa', 'Master\SiswaController@SearchData');

    //Kelas
    Route::resource('master/kelas', 'Master\KelasController');
    Route::post('master/search_kelas','Master\KelasController@SearchData');

    //Jenis Iuran
    Route::resource('master/jenis_iuran', 'Master\JenisIuranController');
    Route::post('master/search_jenis_iuran', 'Master\JenisIuranController@SearchData');

    //Jenis Iuran Waktu
    Route::resource('master/jenis_iuran_waktu', 'Master\JenisIuranWaktuController');
    Route::post('master/search_jenis_iuran_waktu', 'Master\JenisIuranWaktuController@SearchData');

    //Jenis Iuran Kelas
    Route::resource('master/jenis_iuran_kelas', 'Master\JenisIuranKelasController');
    Route::post('master/search_jenis_iuran_kelas', 'Master\JenisIuranKelasController@SearchData');

    // Laporan
    Route::get('laporan/umum', 'Laporan\LaporanController@Umum')->name('laporan/umum');
});

