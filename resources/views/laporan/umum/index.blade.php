@extends('layouts.app', ['title' => __('Data Jenis Iuran')])

@section('content')
    @include('laporan.umum.partials.header', ['title' => __('Laporan Umum')])


    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            {{-- <div class="col-3">
                                <h3 class="mb-0">{{ __('Data Laporan') }}</h3>
                            </div> --}}
                            <div class="col-sm-auto">

                                <form method="get" action="{{ url('laporan/umum') }}" autocomplete="off">
                                    {{-- @csrf --}}

                                            <div class="form-group">
                                                <div class="input-group input-group-alternative">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text"><i class="ni ni-calendar-grid-58"></i></span>
                                                    </div>
                                                    <input class="form-control datepicker" name="start_date" placeholder="Pilih Tanggal" type="text" value="{{$start_date}}" autofocus>
                                                </div>
                                            </div>
                                            <button type="submit" class="btn btn-info">{{ __('Cari') }}</button>

                                            {{-- <a href="{{ route('commodities.create') }}" class="btn btn-info">
                                                <i class="fas fa-search"></i> {{ __('Cari') }}
                                            </a> --}}
                                            <a href="#" class="btn btn-success" target="_blank">
                                                <i class="fas fa-print"></i> {{ __('Cetak') }}
                                            </a>
                                </form>
                            </div>
                                <div class="col-sm-8 text-right mt-2">
                                    <h3 class="mb-0 btn btn-sm btn-white">
                                        <span class="badge badge-pill badge-success">Total IN</span> Rp. {{ number_format($total_in, 0,',','.') }}
                                    </h3>

                                    <h3 class="mb-0 btn btn-sm btn-white">
                                        <span class="badge badge-pill badge-danger">Total Out</span> Rp. 
                                    </h3>
                                </div>
                        </div>
                    </div>

                    <div class="col-12">
                        @if (session('status'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                {{ session('status') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif
                    </div>

                    <div class="table-responsive">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">{{ __('No') }}</th>
                                    <th scope="col">{{ __('Jenis Transaksi') }}</th>
                                    <th scope="col">{{ __('Jumlah') }}</th>
                                    <th scope="col">{{ __('Deskripsi') }}</th>
                                    <th scope="col">{{ __('Tanggal Transaksi') }}</th>
                                    <th scope="col">{{ __('Operator') }}</th>
                                </tr>
                            </thead>
                            <tbody>
                               @foreach($transaction_in as $row)
                                <tr>
                                    <td>{{$no++}}</td>
                                    <td>{{$row->jenis_iuran_kelas_id}}</td>
                                    <td>{{number_format($row->jumlah, 0,',','.')}}</td>
                                    <td>{{$row->description}}</td>
                                    <td>{{$row->transaction_date->format('d M Y')}}</td>
                                    <td>{{$row->created_by}}</td>
                                </tr>
                               @endforeach
                            </tbody>
                        </table>
                    </div>

                    <div class="card-footer py-4">
                        <nav class="d-flex justify-content-center" aria-label="...">
                           
                        </nav>
                    </div>
                </div>
            </div>
        </div>

        @include('layouts.footers.auth')
    </div>
@endsection

@push('js')
<script src="{{ asset('argon') }}/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<script>

    $(document).ready(function () {

       

    });

</script>

@endpush
