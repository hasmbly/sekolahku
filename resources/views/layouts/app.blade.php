<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <meta name="csrf-token" content="{{ csrf_token() }}">

        @if (isset($title) && $title)
            <title>{{ config('app.name', 'Argon Dashboard') }} | {{ $title }}</title>
        @else
            <title>{{ config('app.name', 'Argon Dashboard') }}</title>
        @endif

        <!-- Favicon -->
        <link href="{{ asset('argon') }}/img/brand/logo-sekolah.jpeg" rel="icon" type="image/png">
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
        <!-- Icons -->
        <link href="{{ asset('argon') }}/vendor/nucleo/css/nucleo.css" rel="stylesheet">
        <link href="{{ asset('argon') }}/vendor/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet">
        <!-- Argon CSS -->
        <link type="text/css" href="{{ asset('argon') }}/css/argon.css?v=1.0.0" rel="stylesheet">
        <!-- Style Css -->
        <link href="{{asset('style.css')}}" rel="stylesheet" >
    </head>
    <body class="{{ $class ?? '' }}">
        @auth()
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
            @include('layouts.navbars.sidebar')
        @endauth

        <div class="main-content">
            @include('layouts.navbars.navbar')
            @yield('content')
        </div>

        <div class="box_loader" style="display: none">
            <div class="loader"></div>
        </div>

        @guest()
            @include('layouts.footers.guest')
        @endguest

        <script src="{{ asset('argon') }}/vendor/jquery/dist/jquery.min.js"></script>
        <script src="{{ asset('argon') }}/vendor/bootstrap/dist/js/bootstrap.bundle.min.js"></script>

        @stack('js')

        <!-- Argon JS -->
        <script src="{{ asset('argon') }}/js/argon.js?v=1.0.0"></script>

        {{-- Show Loading --}}
        <script src="{{ asset('argon') }}/js/sekolahku/showLoading.js"></script>

        {{-- pagination --}}
        <script src="{{ asset('argon') }}/js/sekolahku/pagination.js"></script>

        {{-- modal --}}
        <script src="{{ asset('argon') }}/js/sekolahku/modal.js"></script>

        {{-- searchDropdown --}}
        <script src="{{ asset('argon') }}/js/sekolahku/searchDropdown.js"></script>
    </body>
</html>
