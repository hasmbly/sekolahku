<nav class="navbar navbar-vertical fixed-left navbar-expand-md navbar-light bg-white" id="sidenav-main">
    <div class="container-fluid">
        <!-- Toggler -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <!-- Brand -->
        <a class="navbar-brand pt-2" href="{{ route('home') }}">
            <img alt="Apotek Mita Farma" src="{{ asset('argon') }}/img/brand/logo-sekolah.jpeg">
            <br />
            <h3><strong>MTs. Nurul Islam Cisauk</strong></h3>
        </a>
        <!-- User -->
        <ul class="nav align-items-center d-md-none">
            <li class="nav-item dropdown">
                <a class="nav-link" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <div class="media align-items-center">
                        <span class="avatar avatar-sm rounded-circle">
                            <img alt="Image placeholder" src="{{ asset('argon') }}/img/theme/blank_user_photo.png">
                        </span>
                    </div>
                </a>
                <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-right">
                    <div class=" dropdown-header noti-title">
                        <h6 class="text-overflow m-0">{{ __('Welcome!') }}</h6>
                    </div>
                    <a href="{{ route('profile.edit') }}" class="dropdown-item">
                        <i class="ni ni-single-02"></i>
                        <span>{{ __('My profile') }}</span>
                    </a>
                    <div class="dropdown-divider"></div>
                    <a href="{{ route('logout') }}" class="dropdown-item" onclick="event.preventDefault();
                    document.getElementById('logout-form').submit();">
                        <i class="ni ni-user-run"></i>
                        <span>{{ __('Logout') }}</span>
                    </a>
                </div>
            </li>
        </ul>
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
            <!-- Collapse header -->
            <div class="navbar-collapse-header d-md-none">
                <div class="row">
                    <div class="col-6 collapse-brand">
                        <a href="{{ route('home') }}">
                            {{-- <img src="{{ asset('argon') }}/img/brand/blue.png"> --}}
                        </a>
                    </div>
                    <div class="col-6 collapse-close">
                        <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#sidenav-collapse-main" aria-controls="sidenav-main" aria-expanded="false" aria-label="Toggle sidenav">
                            <span></span>
                            <span></span>
                        </button>
                    </div>
                </div>
            </div>

            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link {{ Route::is('home') ? 'active' : '' }}" href="{{ route('home') }}">
                        <i class="ni ni-tv-2 text-primary"></i> {{ __('Dashboard') }}
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ Request::is('pembayaran') ? 'active' : '' }}" href="{{ url('pembayaran') }}">
                        <i class="ni ni-money-coins text-default"></i> {{ __('Cek Pembayaran') }}
                    </a>
                </li>
            </ul>

            <!-- Divider -->
            <hr class="my-3">
            <!-- Heading -->
            <h6 class="navbar-heading text-muted">Manajemen</h6>
            <!-- Navigation -->
                <ul class="navbar-nav mb-md-3">
                    <li class="nav-item">
                        <a class="nav-link {{ Route::is('transactions.report.penjualan') ? 'active' : '' }}" href="#laporan" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="navbar-examples">
                            <i class="ni ni-single-copy-04 text-info"></i>
                            <span class="nav-link-text">{{ __('Laporan') }}</span>
                        </a>
                    <div class="collapse {{ Request::is('laporan/*') ? 'show' : '' }}" id="laporan">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                                <a class="nav-link {{ Request::is('laporan/umum') ? 'active' : '' }}" href="{{ url('laporan/umum') }}">
                                    {{ __('Umum') }}
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#">
                                    {{ __('Bayaran') }}
                                </a>
                            </li>
                            @if ( auth()->user()->hasRole("SUPER_ADMIN") )
                            {{-- <li class="nav-item">
                                <a class="nav-link" href="{{ route('transactions.report.pembelian', 2) }}">
                                    <i class="ni ni-box-2 text-warning"></i> {{ __('Pembelian') }}
                                </a>
                            </li> --}}
                            @endif
                        </ul>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ Request::is('master/*') ? 'active' : '' }}" href="#data_master" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="data_master">
                        <i class="ni ni-books text-warning"></i>
                        <span class="nav-link-text">{{ __('Data Master') }}</span>
                    </a>
                    <div class="collapse {{ Request::is('master/*') ? 'show' : '' }}" id="data_master">
                        <ul class="nav nav-sm flex-column">
                            <li class="nav-item">
                                <a class="nav-link {{ Request::is('master/siswa') ? 'active' : '' }}" href="{{ url('master/siswa') }}">
                                    {{ __('Siswa') }}
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ Request::is('master/kelas') ? 'active' : '' }}" href="{{ url('master/kelas') }}">
                                    {{ __('Kelas') }}
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ Request::is('master/jenis_iuran') ? 'active' : '' }}" href="{{ url('master/jenis_iuran') }}">
                                    {{ __('Jenis Iuran') }}
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ Request::is('master/jenis_iuran_waktu') ? 'active' : '' }}" href="{{ url('master/jenis_iuran_waktu') }}">
                                    {{ __('Jenis Iuran Waktu') }}
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link {{ Request::is('master/jenis_iuran_kelas') ? 'active' : '' }}" href="{{ url('master/jenis_iuran_kelas') }}">
                                    {{ __('Jenis Iuran Kelas') }}
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>
