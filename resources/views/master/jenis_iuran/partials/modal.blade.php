<!-- Modal Add -->
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modal-header">Tambah Jenis Iuran</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post" action="{{ url('master/jenis_iuran') }}" autocomplete="off" id="form">
            @csrf
            <div id="method"></div>

            <div id="hide-for-delete">
                <div class="form-group">
                    <label class="form-control-label" for="input-jenis-iuran-waktu">Jenis Iuran Waktu</label>
                    <select class="form-control form-control-alternative" id="input-jenis-iuran-waktu" name="jenis_iuran_waktu_id" placeholder="Pilih Satuan Barang">
                        @foreach ( $list_jenis_iuran_waktu as $jenis_iuran_waktu )
                            <option value="{{ $jenis_iuran_waktu->id }}">{{ $jenis_iuran_waktu->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label class="form-control-label" for="input-jenis-iuran-nama">Nama Jenis Iuran</label>
                    <input type="text" class="form-control form-control-alternative" id="input-jenis-iuran-nama" name="name" placeholder="Masukan nama Jenis Iuran">
                </div>
                <div class="form-group">
                    <label class="form-control-label" for="input-jenis-iuran-description">Deskripsi Jenis Iuran</label>
                    <input type="textarea" class="form-control form-control-alternative" id="input-jenis-iuran-description" name="description" rows="3" placeholder="Deskripsi Jenis Iuran"></textarea>
                </div>
            </div>

            <button type="submit" style="display: none;" class="btn btn-primary" id="btnSubmitAdd">Submit</button>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="button-simpan" onclick="document.getElementById('btnSubmitAdd').click()">Simpan</button>
      </div>
    </div>
  </div>
</div>
