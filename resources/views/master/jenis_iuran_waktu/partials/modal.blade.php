<!-- Modal Add -->
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modal-header">Tambah Jenis Iuran Waktu</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post" action="{{ url('master/jenis_iuran_waktu') }}" autocomplete="off" id="form">
            @csrf
            <div id="method"></div>

            <div id="hide-for-delete">
                <div class="form-group">
                    <label class="form-control-label" for="input-jenis-iuran-waktu-nama">Nama Jenis Iuran Waktu</label>
                    <input type="text" class="form-control form-control-alternative" id="input-jenis-iuran-waktu-nama" name="name" placeholder="Masukan nama Jenis Iuran Waktu">
                </div>
            </div>

            <button type="submit" style="display: none;" class="btn btn-primary" id="btnSubmitAdd">Submit</button>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="button-simpan" onclick="document.getElementById('btnSubmitAdd').click()">Simpan</button>
      </div>
    </div>
  </div>
</div>
