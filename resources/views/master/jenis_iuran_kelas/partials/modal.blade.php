<!-- Modal Add -->
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modal-header">Tambah Jenis Iuran Kelas</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post" action="{{ url('master/jenis_iuran_kelas') }}" autocomplete="off" id="form">
            @csrf
            <div id="method"></div>

            <div id="hide-for-delete">
                <div class="form-group">
                    <label class="form-control-label" for="input-jenis-iuran">Jenis Iuran</label>
                    <select class="form-control form-control-alternative" id="input-jenis-iuran" name="jenis_iuran_id" placeholder="Pilih Jenis Iuran">
                        @foreach ( $list_jenis_iuran as $jenis_iuran )
                            <option value="{{ $jenis_iuran->id }}">{{ $jenis_iuran->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label class="form-control-label" for="input-kelas">Kelas</label>
                    <select class="form-control form-control-alternative" id="input-kelas" name="kelas_id" placeholder="Pilih Kelas">
                        @foreach ( $list_kelas as $kelas )
                            <option value="{{ $kelas->id }}">{{ $kelas->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label class="form-control-label" for="input-amount">Nilai Bayaran</label>
                    <input type="text" class="form-control form-control-alternative" id="input-amount" name="amount" placeholder="Masukan Nilai Bayaran">
                </div>
                <div class="form-group">
                    <label class="form-control-label" for="input-angkatan">Angkatan</label>
                    <input type="text" class="form-control form-control-alternative" id="input-angkatan" name="angkatan" placeholder="Masukan Tahun Angkatan">
                </div>
            </div>

            <button type="submit" style="display: none;" class="btn btn-primary" id="btnSubmitAdd">Submit</button>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="button-simpan" onclick="document.getElementById('btnSubmitAdd').click()">Simpan</button>
      </div>
    </div>
  </div>
</div>
