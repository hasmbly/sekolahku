@extends('layouts.app', ['title' => __('Data Kelas')])

@section('content')
    @include('master.kelas.partials.header', ['title' => __('Data Kelas')])

    @include('master.kelas.partials.modal')

    <div class="container-fluid mt--7">
        <div class="row">
            <div class="col">
                <div class="card shadow data-box">

                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col-8">
                            </div>
                            <div class="col-12 text-right">

                                <a href="#" data-toggle="modal" id="button-tambah" data-target="#modal" class="btn btn-primary float-right"><i class="fa fa-plus"></i> {{ __('Tambah') }}</a>

                                <!-- Search form -->
                                <div class="input-group float-right input-group-alternative mb-4" style="width: 200px; margin-right:10px;">
                                    <input name="" id="text-search" type="text" class="form-control form-control-alternative" placeholder="Search..." aria-label="" aria-describedby="search">
                                    <div class="input-group-append">
                                        <span class="input-group-text" id="search"><i class="fa fa-search"></i></span>
                                    </div>
                                </div>

                                <!-- Filter by option -->

                            </div>
                        </div>
                    </div>

                    <div class="col-12">
                        {{-- Alert Status : Create, Edit --}}
                        <!-- Notif for search datatable -->
                        <div class="notif-msg" id="notif-msg"></div>

                        @if (session('error_message') || session('success_message'))
                            <div class="alert {{ session('error_message') ? ' alert-danger' : 'alert-success' }} alert-dismissible fade show" role="alert">
                                {{ session('error_message') ? session('error_message') : session('success_message') }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endif

                    </div>

                    <div class="table-responsive card-body">
                        <table class="table align-items-center table-flush">
                            <thead class="thead-light" id="header">
                                <tr>
                                    <th scope="col">{{ __('No') }}</th>
                                    <th scope="col">{{ __('Nama') }}</th>
                                    {{-- Action --}}
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($result as $row)
                                    <tr>
                                        <td>{{ $no++ }}</td>
                                        <td>{{ $row->name }}</td>
                                        <td class="text-right">
                                            <a href="#" class="btn btn-sm btn-primary edit" data-id={{ $row->id }}><i class="fas fa-pencil-alt"></i> Edit</a>
                                            <a href="#" class="btn btn-sm btn-danger delete" data-id={{ $row->id }}><i class="fas fa-trash"></i>Delete</a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                    <input type="hidden" name="total_page" id="total_page" value="{{ $total_page }}">
                    <input type="hidden" name="old_search_text" id="old_search_text" value="">

                    <div class="card-footer text-right">
                        <nav class="d-flex justify-content-end" aria-label="...">
                            <div id="pagination">
                                {{ $result->links() }}
                            </div>
                        </nav>
                    </div>

                </div>
            </div>
        </div>

        @include('layouts.footers.auth')
    </div>
@endsection

@push('js')

<script>
    $(document).ready(function () {
        pagination.init({
            csrf_token: '{{ csrf_token() }}',
            colName: ['name'], // <-- nama nama kolom yang sama persis dgn table di database
            max_row: '{{ $max_row }}',
            url: '{{ url("master/search_kelas") }}'
        });
        modal.init({
            url: '{{ url("master/kelas") }}',
            headerMessageModalAdd: 'Tambah Kelas',
            headerMessageModalEdit: 'Edit Kelas'
        });
    });
</script>

@endpush
