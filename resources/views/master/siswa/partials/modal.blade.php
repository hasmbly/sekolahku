<!-- Modal Add -->
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modal-header">Tambah Siswa</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post" action="{{ url('master/siswa') }}" autocomplete="off" id="form">
            @csrf
            <div id="method"></div>

            <div id="hide-for-delete">
                <div class="form-group">
                    <label class="form-control-label" for="input-kelas">Kelas</label>
                    <select class="form-control form-control-alternative" id="input-kelas" name="kelas_id" placeholder="Pilih Kelas">
                        @foreach ( $list_kelas as $kelas )
                            <option value="{{ $kelas->id }}">{{ $kelas->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label class="form-control-label" for="input-nama">Nama</label>
                    <input type="text" class="form-control form-control-alternative" id="input-nama" name="nama" placeholder="Masukan Nama">
                </div>
                <div class="form-group">
                    <label class="form-control-label" for="input-nis">NIS</label>
                    <input type="text" class="form-control form-control-alternative" id="input-nis" name="nis" placeholder="Masukan NIS">
                </div>
                <div class="form-group">
                    <label class="form-control-label" for="input-jenis-kelamin">Jenis Kelamin</label>
                    <select class="form-control form-control-alternative" id="input-jenis-kelamin" name="jenis_kelamin" placeholder="Pilih Jenis Kelamin">
                        <option value="L">Laki - laki</option>
                        <option value="P">Perempuan</option>
                    </select>
                </div>
                <div class="form-group">
                    <label class="form-control-label" for="input-tempat-tanggal-lahir">Tempat Tanggal Lahir</label>
                    <input type="text" class="form-control form-control-alternative" id="input-tempat-tanggal-lahir" name="tempat_tgl_lahir" placeholder="Masukan Tempat Tanggal Lahir">
                </div>
                <div class="form-group">
                    <label class="form-control-label" for="input-agama">Agama</label>
                    <input type="text" class="form-control form-control-alternative" id="input-agama" name="agama" placeholder="Masukan Agama">
                </div>
                <div class="form-group">
                    <label class="form-control-label" for="input-alamat">alamat</label>
                    <input type="text" class="form-control form-control-alternative" id="input-alamat" name="alamat" placeholder="Masukan Alamat">
                </div>
                <div class="form-group">
                    <label class="form-control-label" for="input-no-tlp">No. Telephone</label>
                    <input type="text" class="form-control form-control-alternative" id="input-no-tlp" name="no_tlp" placeholder="Masukan Nomor Tlp/Hp">
                </div>
                <div class="form-group">
                    <label class="form-control-label" for="input-nama-ortu">Nama Orang Tua</label>
                    <input type="text" class="form-control form-control-alternative" id="input-nama-ortu" name="nama_orang_tua" placeholder="Masukan Nama Orang Tua" />
                </div>
                <div class="form-group">
                    <label class="form-control-label" for="input-pekerjaan-ortu">Pekerjaan Orang Tua</label>
                    <input type="text" class="form-control form-control-alternative" id="input-pekerjaan-ortu" name="pekerjaan_orang_tua" placeholder="Masukan Pekerjaan Orang Tua" />
                </div>
                <div class="form-group">
                    <label class="form-control-label" for="input-asal-sekolah">Asal Sekolah</label>
                    <input type="text" class="form-control form-control-alternative" id="input-asal-sekolah" name="asal_sekolah" placeholder="Masukan Nomor Tlp/Hp">
                </div>
                <div class="form-group">
                    <label class="form-control-label" for="input-tanggal-masuk">Tanggal Masuk</label>
                    <div class="input-group input-group-alternative">
                        <div class="input-group-prepend">
                            <span class="input-group-text"><i class="ni ni-calendar-grid-58"></i></span>
                        </div>
                        <input class="form-control datepicker" id="input-tanggal-masuk" name="tanggal_masuk" placeholder="Pilih Tanggal Masuk" type="text" autofocus>
                    </div>
                </div>
            </div>

            <button type="submit" style="display: none;" class="btn btn-primary" id="btnSubmitAdd">Submit</button>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="button-simpan" onclick="document.getElementById('btnSubmitAdd').click()">Simpan</button>
      </div>
    </div>
  </div>
</div>
