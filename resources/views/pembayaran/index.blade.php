@extends('layouts.app', ['title' => __('Pembayaran')])

@section('content')
    @include('pembayaran.partials.cards')

    @include('pembayaran.partials.modal')

    <div class="container-fluid mt--7">
        <div class="row mb-3">
            <div class="col-xl-4 order-xl-2 mb-5 mb-xl-0">
                @include('pembayaran.partials.profile_siswa')
            </div>
            <div class="col-xl-6 order-xl-1">
                <div class="card shadow data-box">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col">
                            <h6 class="text-uppercase ls-1 mb-1">Iuran &nbsp; <span class="badge badge-info">TAHUNAN</span></h6>
                            </div>
                            <div class="col">
                                <ul class="justify-content-end">
                                    {{-- <a href="#" data-toggle="modal" id="button-tambah" data-target="#modal" class="btn btn-primary float-right">{{ __('Bayar') }}</a> --}}
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="notif-msg" id="notif-msg"></div>

                    @if (session('error_message') || session('success_message'))
                        <div class="alert {{ session('error_message') ? ' alert-danger' : 'alert-success' }} alert-dismissible fade show" role="alert">
                            {{ session('error_message') ? session('error_message') : session('success_message') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif

                    <div classs="load-table"></div>

                    <div class="table-responsive">
                        <table class="table align-items-center table-flush main-table border-0 iuran-table-main">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">{{ __('Nama') }}</th>
                                    <th scope="col">{{ __('Nilai') }}</th>
                                    <th scope="col">{{ __('Status') }}</th>
                                    <th scope="col">{{ __('Jumlah') }}</th>
                                    <th scope="col">{{ __('Tgl Bayar') }}</th>
                                    <th scope="col">{{ __('Operator') }}</th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody class="iuran-table" id="accordion">
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
            <div class="col-xl-6 order-xl-1">
                <div class="card shadow data-box">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col">
                            <h6 class="text-uppercase ls-1 mb-1">Iuran &nbsp; <span class="badge badge-info">BULANAN</span></h6>
                            </div>
                            <div class="col">
                                <ul class="justify-content-end">
                                    {{-- <a href="#" data-toggle="modal" id="button-tambah" data-target="#modal" class="btn btn-primary float-right">{{ __('Bayar') }}</a> --}}
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="notif-msg" id="notif-msg"></div>

                    @if (session('error_message') || session('success_message'))
                        <div class="alert {{ session('error_message') ? ' alert-danger' : 'alert-success' }} alert-dismissible fade show" role="alert">
                            {{ session('error_message') ? session('error_message') : session('success_message') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif

                    <div classs="load-table"></div>

                    <div class="table-responsive">
                        <table class="table align-items-center table-flush main-table border-0 iuran-table-main">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">{{ __('Nama') }}</th>
                                    <th scope="col">{{ __('Nilai') }}</th>
                                    <th scope="col">{{ __('Status') }}</th>
                                    <th scope="col">{{ __('Jumlah') }}</th>
                                    <th scope="col">{{ __('Tgl Bayar') }}</th>
                                    <th scope="col">{{ __('Operator') }}</th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody class="iuran-table" id="accordion">
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
        <div class="row mb-3">
            {{-- <div class="col-xl-4 order-xl-2 mb-5 mb-xl-0">
                @include('pembayaran.partials.profile_siswa')
            </div> --}}
        </div>
        <div class="row">
            <div class="col-xl-6 order-xl-1">
                <div class="card shadow data-box">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col">
                            <h6 class="text-uppercase ls-1 mb-1">Iuran &nbsp; <span class="badge badge-info">SEMESTER</span></h6>
                            </div>
                            <div class="col">
                                <ul class="justify-content-end">
                                    {{-- <a href="#" data-toggle="modal" id="button-tambah" data-target="#modal" class="btn btn-primary float-right">{{ __('Bayar') }}</a> --}}
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="notif-msg" id="notif-msg"></div>

                    @if (session('error_message') || session('success_message'))
                        <div class="alert {{ session('error_message') ? ' alert-danger' : 'alert-success' }} alert-dismissible fade show" role="alert">
                            {{ session('error_message') ? session('error_message') : session('success_message') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif

                    <div classs="load-table"></div>

                    <div class="table-responsive">
                        <table class="table align-items-center table-flush main-table border-0 iuran-table-main">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">{{ __('Nama') }}</th>
                                    <th scope="col">{{ __('Nilai') }}</th>
                                    <th scope="col">{{ __('Status') }}</th>
                                    <th scope="col">{{ __('Jumlah') }}</th>
                                    <th scope="col">{{ __('Tgl Bayar') }}</th>
                                    <th scope="col">{{ __('Operator') }}</th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody class="iuran-table" id="accordion">
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
            <div class="col-xl-6 order-xl-1">
                <div class="card shadow data-box">
                    <div class="card-header border-0">
                        <div class="row align-items-center">
                            <div class="col">
                            <h6 class="text-uppercase ls-1 mb-1">Iuran &nbsp; <span class="badge badge-info">BEBAS</span></h6>
                            </div>
                            <div class="col">
                                <ul class="justify-content-end">
                                    {{-- <a href="#" data-toggle="modal" id="button-tambah" data-target="#modal" class="btn btn-primary float-right">{{ __('Bayar') }}</a> --}}
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="notif-msg" id="notif-msg"></div>

                    @if (session('error_message') || session('success_message'))
                        <div class="alert {{ session('error_message') ? ' alert-danger' : 'alert-success' }} alert-dismissible fade show" role="alert">
                            {{ session('error_message') ? session('error_message') : session('success_message') }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endif

                    <div classs="load-table"></div>

                    <div class="table-responsive">
                        <table class="table align-items-center table-flush main-table border-0 iuran-table-main">
                            <thead class="thead-light">
                                <tr>
                                    <th scope="col">{{ __('Nama') }}</th>
                                    <th scope="col">{{ __('Nilai') }}</th>
                                    <th scope="col">{{ __('Status') }}</th>
                                    <th scope="col">{{ __('Jumlah') }}</th>
                                    <th scope="col">{{ __('Tgl Bayar') }}</th>
                                    <th scope="col">{{ __('Operator') }}</th>
                                    <th scope="col"></th>
                                </tr>
                            </thead>
                            <tbody class="iuran-table" id="accordion">
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
        @include('layouts.footers.auth')
    </div>
@endsection

@push('js')

<script>

    $(document).ready(function () {

        // initialize search
        searchDropdown.init({
            url: '{{ url("master/search_siswa") }}',
            urlOnClick: '{{ url("master/siswa") }}',
            csrf_token: '{{ csrf_token() }}',
            loadDataToTable: true
        });

        // initialize modal
        // modal.init({
        //     url: '{{ url("master/siswa") }}',
        //     headerMessageModalAdd: 'Tambah Siswa',
        //     headerMessageModalEdit: 'Edit Siswa'
        // });

    });

</script>
    {{-- <script src="{{ asset('argon') }}/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script> --}}
@endpush
