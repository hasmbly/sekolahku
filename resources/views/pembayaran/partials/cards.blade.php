<div class="header bg-gradient-green pb-8 pt-5 pt-md-7">
    <div class="container-fluid">
        <div class="header-body">
            <!-- Card stats -->
            <div class="row">
                <div class="col-xl-4 col-lg-6">
                    <div class="card bg-gradient-neutral card-stats mb-4 mb-xl-0">
                        <div class="card-body">
                            {{-- <div class="text-left text-muted mb-4">
                                <div class="text-black">
                                    <h3>
                                        <strong>
                                            Cek Pembayaran Siswa
                                        </strong>
                                    </h3>
                                </div>
                            </div> --}}

                            <div class="notif-msg" id="notif-msg"></div>

                            <div class="row">
                                <div class="col-sm-2 text-right pr-0">
                                    <div class="media align-items-center">
                                        <span class="avatar avatar-sm rounded-circle">
                                            <img alt="Image placeholder" src="{{ asset('argon') }}/img/brand/search-siswa.png">
                                        </span>
                                    </div>
                                </div>
                                <div class="col-sm text-left pl-0">
                                    <a class="nav-link pr-0" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <div class="form-group mb-0">
                                            <div class="input-group input-group-alternative">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text"><i class="fas fa-search"></i></span>
                                                </div>
                                                <input class="form-control search-items" placeholder="Masukan Nama atau NIS Siswa" type="text">

                                                <div class="dropdown-menu dropdown-menu-arrow dropdown-menu-left search-dropdown-items">
                                                    <div class=" dropdown-header noti-title">
                                                        <h6 class="text-overflow m-0">{{ __('List Siswa!') }}</h6>
                                                    </div>
                                                    <div class="list-items"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </div>


                            {{-- <div class="form-group mb-4">
                                <div class="input-group display-4 input-group-alternative">
                                    <input class="form-control form-control-alternative" name="" id="text-search" type="text" placeholder="Silahkan Masukan NIS atau Nama" aria-label="" aria-describedby="search" autofocus>
                                    <div class="input-group-append">
                                        <span class="input-group-text" id="search"><i class="fa fa-search"></i></span>
                                    </div>
                                </div>
                            </div> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
