<!-- Modal Add -->
<div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" data-backdrop="static" data-keyboard="false">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modal-header">Tambah Pembayaran</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post" action="{{ url('pembayaran') }}" autocomplete="off" id="form">
            @csrf

            {{-- for transaction_in --}}
            <input type="hidden" name="siswa_id" id="siswa_id" />
            <input type="hidden" name="jenis_iuran_kelas_id" id="jenis_iuran_kelas_id" />
            <input type="hidden" name="created_by" value="{{ auth()->user()->nama }}" />
            <input type="hidden" name="transaction_date" value="{{ date('Y-m-d') }}" />

            {{-- for cicilans --}}
            <input type="hidden" name="transactions_in_id" id="transactions_in_id" />
            <input type="hidden" name="cicilan_date" value="{{ date('Y-m-d') }}" />

            <div id="hide-for-delete">
                <div class="form-group">
                    <label class="form-control-label" for="input-jenis-iuran-kelas">Jenis Iuran Kelas</label>
                    <select class="form-control form-control-alternative" id="input-jenis-iuran-kelas" name="jenis_iuran_kelas_id" placeholder="Pilih Jenis Iuran Kelas" onchange="document.getElementById('input-nilai-iuran').value = this.selectedOptions[0].getAttribute('data-total')" disabled>
                        <option>-- Pilih Jenis Iuran Kelas --</option>
                        @if (!empty($list_jenis_iuran_kelas))
                            @foreach ( $list_jenis_iuran_kelas as $jenis_iuran_kelas )
                                <option value="{{ $jenis_iuran_kelas->id }}" data-total="{{ $jenis_iuran_kelas->amount }}">{{ $jenis_iuran_kelas->jenisIuran->name . ' - ' . $jenis_iuran_kelas->kelas->name . ' (' . $jenis_iuran_kelas->angkatan . ')' }}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
                <div class="form-group">
                    <label class="form-control-label" for="input-nilai-iuran">Nilai Iuran</label>
                    <input type="number" min="0" class="form-control form-control-alternative" id="input-nilai-iuran" required autofocus disabled>
                </div>
                <div class="form-group">
                    <label class="form-control-label" for="input-status">Metode Bayar</label>
                    <select class="form-control form-control-alternative" id="input-status" name="status" placeholder="Pilih Status">
                        <option value="LUNAS">LUNAS</option>
                        <option value="CICILAN">CICILAN</option>
                    </select>
                </div>
                <div class="form-group">
                    <label class="form-control-label" for="input-jumlah">Tunai</label>
                    <input type="number" min="0" class="form-control form-control-alternative" id="input-jumlah" name="jumlah" placeholder="Masukan Jumlah Bayar" required autofocus>
                </div>
                {{-- <div class="form-group">
                    <label class="form-control-label" for="input-change">Kembali</label>
                    <input type="hidden" name="change" id="input-change-real" class="form-control form-control-muted" required autofocus disabled>

                    <input type="text" name="change-display" id="input-change" class="form-control form-control-muted" required autofocus disabled>
                </div> --}}
            </div>

            <button type="submit" style="display: none;" class="btn btn-primary" id="btnSubmitSave">Submit</button>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="button-save" onclick="document.getElementById('btnSubmitSave').click()">Simpan</button>
      </div>
    </div>
  </div>
</div>
