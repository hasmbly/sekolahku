<div class="card card-profile shadow">
    <div class="row justify-content-center">
        <div class="col-lg-3 order-lg-2">
            <div class="card-profile-image">
                <a href="#">
                    <img src="{{ asset('argon') }}/img/theme/blank_user_photo.png" class="rounded-circle">
                </a>
            </div>
        </div>
    </div>
    <div class="card-header text-center border-0 pt-8 pt-md-4 pb-0 pb-md-4">
    </div>
    <div class="card-body pt-0 pt-md-4">
        <div class="row">
            <div class="col">
                <div class="card-profile-stats d-flex justify-content-center mt-md-6">
                    <div>
                        <span class="heading"><div id="kelas"></div></span>
                        <span class="description"><div id="kelasDescription"></div></span>
                    </div>
                    <div>
                        <span class="heading"><div id="jenisKelamin"></div></span>
                        <span class="description"><div id="jenisKelaminkelasDescription"></div></span>
                    </div>
                    <div>
                        <span class="heading"><div id="agama"></div></span>
                        <span class="description"><div id="agamaDescription"></div></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="text-center mt-2">
            <h3>
                <div id="nama"></div>
            </h3>
            <div class="h5 font-weight-300">
                <div id="tempat-tanggal-lahir"></div>
            </div>

            <div class="card-profile-stats d-flex justify-content-center">
                <div>
                    <span class="h5 mt-2"><div id="nis"></div></span>
                    <span class="description"><div id="nisDescription"></div></span>
                </div>
                <div>
                    <span class="h5 mt-2"><div id="noTlp"></div></span>
                    <span class="description"><div id="noTlpDescription"></div></span>
                </div>
            </div>

            <div class="card-profile-stats d-flex justify-content-center">
                <div>
                    <span class="h5 mt-2"><div id="alamat"></div></span>
                    <span class="description"><div id="alamatMasukDescription"></div></span>
                </div>
            </div>

            <hr class="my-4" />

            <div class="card-profile-stats d-flex justify-content-center">
                <div>
                    <span class="h5 mt-2"><div id="asalSekolah"></div></span>
                    <span class="description"><div id="asalSekolahDescription"></div></span>
                </div>
                <div>
                    <span class="h5 mt-2"><div id="tglMasuk"></div></span>
                    <span class="description"><div id="tglMasukDescription"></div></span>
                </div>
            </div>

            <div class="card-profile-stats d-flex justify-content-center">
                <div>
                    <span class="h5 mt-2"><div id="ortuDetail"></div></span>
                    <span class="description"><div id="ortuDetailMasukDescription"></div></span>
                </div>
            </div>
        </div>
    </div>
</div>
