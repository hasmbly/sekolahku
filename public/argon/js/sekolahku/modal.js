'use strict'

var modal = (function () {

    // property
    var config =
    {
        // default
        modal: $('#modal'),
        table: $(".table"),
        form: $("#form"),
        buttonAdd: $("#button-tambah"),
        modalHeader: $('#modal-header'),
        hideForDelete: $('#hide-for-delete'),
        hiddenMethod: $('#method'),
        headerMessageModalDelete: 'Apakah anda yakin untuk menghapus Data ini?',

        // depends
        url: null, // ex: {{ url('master/jenis_iuran') }}
        headerMessageModalAdd: null, // ex: Tambah Jenis Iuran
        headerMessageModalEdit: null, // ex: Edit Jenis Iuran
        mapOfInputElement: {}, // ex: { '#input-jenis-iuran-waktu': 'jenis_iuran_waktu_id' }
    };

    // init
    var init = function (settings) {
        config.url = settings.url;
        config.headerMessageModalAdd = settings.headerMessageModalAdd;
        config.headerMessageModalEdit = settings.headerMessageModalEdit;

        bindFunction();
        setMapOfInputElement();
    };

    // eventListener
    var bindFunction = function () {
        // Event handler button Edit dan Delete
        config.table
            .on("click", "a.edit", function () {
                var id = $(this).data('id');

                // Call AJAX show function
                show(id);
            })
            .on("click", "a.delete", function () {
                var id = $(this).data('id');

                // Call AJAX initModal function
                initModal(false, id);
            });

        // event handler button "Tambah"
        config.buttonAdd.click(function () {
            initModalAdd();
        });
    };

    // show function just for update events
    var show = function (id) {
        // show Loading
        showLoading();

        $.ajax({
            type: 'GET',
            url: config.url + "/" + id,
            dataType: 'json',
            success: function (data) {
                initModal(true, data);
            },
            error: function (error) {
                closeLoading();
            },
            complete: function (_xhr) {
                closeLoading();
            }
        });
    };

    // initializing modal for update or delete
    var initModal = function (isModalForUpdate = false, val) {
        // hide or show body
        isModalForUpdate ? config.hideForDelete.show() : config.hideForDelete.hide();

        // change header message
        isModalForUpdate ? config.modalHeader.html(config.headerMessageModalEdit) : config.modalHeader.html(config.headerMessageModalDelete);

        // call setValueOfInputElement() if for 'update'
        isModalForUpdate ? setValueOfInputElement(true, val) : '';

        // rubah atribute 'action' di element form
        isModalForUpdate ? config.form.attr('action', config.url + "/" + val.id) : config.form.attr('action', config.url + "/" + val);

        // masukan input hidden untuk method PUT or Delete
        isModalForUpdate ? config.hiddenMethod.html(setHiddenMethodPost('PUT')) : config.hiddenMethod.html(setHiddenMethodPost('DELETE'));

        // init + show modal
        config.modal.modal();
    };

    var setValueOfInputElement = function (fillOrEmpty, val) {
        $.each(config.mapOfInputElement, function (i, v) {
            if (fillOrEmpty) {
                if (v != null || v != '') {
                    var value = `val.` + v;

                    $(i).val(eval(value));
                }
            }
            else if (!fillOrEmpty) {
                $(i).val('');
            }
        });
    };

    var setHiddenMethodPost = function (val) {
        return `<input type='hidden' name='_method' value='` + val + `'>`;
    };

    var setMapOfInputElement = function () {
        var obj = {};

        $('.form-control').each(function () {
            if ($(this).attr('name') == null || $(this).attr('name') == '') {
                return;
            }

            var key = `#` + $(this).attr('id') + ``;
            var value = $(this).attr('name');

            obj[key] = value;
        });

        config.mapOfInputElement = obj;
    };

    var initModalAdd = function ()
    {
        // change header message
        config.modalHeader.html(config.headerMessageModalAdd);

        // change action url on form element
        config.form.attr('action', config.url);

        // remove div method
        config.hiddenMethod.html('');

        // set value input element
        setValueOfInputElement(false);

        // show body
        config.hideForDelete.show();
    }

    return {
        config: config,
        init: init
    };

})();
