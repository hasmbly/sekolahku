'use strict';

//#region searchDropdown
var searchDropdown = (function () {

    // property
    var config = {
        // default
        inputSearch: $(".search-items"),
        dropDownList: $(".search-dropdown-items"),
        listItem: $('.list-items'),
        table: $('.main-table'),
        timer: null,

        // custom
        url: null,
        loadDataToTable: false,
        csrf_token: null,
        resultID: null,
        urlOnClick: null
    };

    // constructor
    var init = function (settings) {
        config.url = settings.url;
        config.loadDataToTable = settings.loadDataToTable;
        config.csrf_token = settings.csrf_token;
        config.urlOnClick = settings.urlOnClick;

        bindFunction();
    };

    // Bind Function
    var bindFunction = function () {
        $(document)
            .on('click', function () {
                config.dropDownList.hide();
            });

        config.inputSearch
            .on('click', function () {
                config.dropDownList.show();
            })
            .on('keyup', function () {
                // pake timer biar gk banyak dam gk bentrok ajax nya
                clearTimeout(config.timer);
                config.timer = setTimeout(function () {
                    config.dropDownList.show();

                    // getAllData
                    searchItems();

                }, 500);
            });

        config.listItem
            .on('click', 'a.item', function () {
                var id = $(this).data('id');

                config.resultID = id;

                // check if loadDataToTable == true
                if (config.loadDataToTable)
                {
                    // initialize getDataWithID
                    getDataWithID.init({
                        url: config.urlOnClick,
                        id: config.resultID
                    });

                    var data = getDataWithID.config.data;

                    // initialize loadProfileSiswa
                    loadProfileSiswa.init({
                        data: data
                    });

                    if (data.siswa_iuran_info == null || data.siswa_iuran_info.length === 0)
                    {
                        var tahunAngkatan = new Date(data.tanggal_masuk);
                        var urlJenisIuranKelas = `<a href="/master/jenis_iuran_kelas">Jenis Iuran Kelas</a>`;

                        alert_msg('Maaf, Harap Melengkapi Data' + urlJenisIuranKelas + ' Untuk Angkatan ' + tahunAngkatan.getFullYear())
                    }
                    else
                    {
                        console.log("siswa_iuran_info is not Empty");
                    }

                    // create new table base on jenis_iuran_waktu
                    // then init LoadTableIuranSiswa
                    // CreateTableJenisIuranWaktu.init({
                    //     data: data
                    // });

                    // initialize LoadTableIuranSiswa
                    // LoadTableIuranSiswa.init({
                    //     data: data
                    // });
                }
            });
    };

    // get all Data
    var searchItems = function () {
        var value = config.inputSearch.val();

        var postObject = {
            'text_search': value
        }

        getAllData.init({
            url: config.url,
            csrf_token: config.csrf_token,
            postObject: postObject
        });

        var resultData = getAllData;

        contentItems(resultData.config.data);
    };

    var contentItems = function (data)
    {
        config.listItem.html('');

        var container = "<div class='content-items'></div>";

        config.listItem.html(container);

        var result = '';

        var resultData = data.rs_data.data;

        $.each(resultData, function (key, value) {
            result += preparingItems(value);
        });

        $('.content-items').replaceWith(result);
    };

    // preparing items before load into dropdown list
    var preparingItems = function (data)
    {
        var dataWithtHTML = "<a href='#' class='dropdown-item item' data-id=" + data.id + "> \
                    <span>" + data.nama + "</span> \
                    </a>";

        return dataWithtHTML;
    };

    // return
    return {
        config: config,
        init: init
    };

})();
//#endregion

// common

//#region getAllData POST
var getAllData = (function () {
    var config = {
        url: null,
        csrf_token: null,
        postObject: null,
        data: null
    };

    var init = function (settings) {
        config.url = settings.url;
        config.csrf_token = settings.csrf_token;
        config.postObject = settings.postObject;

        getAllData(config.url, config.postObject);
    };

    var getAllData = function (url, postObject)
    {
        var resultData = null;

        $.ajax({
            type: 'POST',
            async: false,
            url: config.url,
            headers: { "X-CSRF-TOKEN": config.csrf_token },
            dataType: 'JSON',
            data: config.postObject,
            success: function (data) {
                resultData = data;
            }
        });

        config.data = resultData;

        console.l
    };

    return {
        config: config,
        init: init
    };

})();
//#endregion

//#region getDataWithID
var getDataWithID = (function ()
{
    var config = {
        url: null,
        data: null,
        id: null
    };

    var init = function (settings)
    {
        config.url = settings.url;
        config.id = settings.id;

        getDataWithID(settings.id);
    };

    var getDataWithID = function (id)
    {
        var resultData = null;

        $.ajax({
            url: config.url + '/' + id,
            method: 'GET',
            async: false,
            data: { id: id },
            type: 'json',
            success: function (data) {
                resultData = data;
            }
        });

        console.log('result-data: ' + resultData);

        config.data = resultData;

        return;
    };

    return {
        config: config,
        init: init
    };

})();
//#endregion

// siswa

//#region CreateTableJenisIuranWaktu
var CreateTableJenisIuranWaktu = (function () {
    var config = {
        data: null
    };

    var init = function (settings) {
        config.data = settings.data;

        setup();
    };

    var setup = function (settings) {

    };

    return {
        config: config,
        init: init
    };

})();
//#endregion


//#region LoadTableIuranSiswa
var LoadTableIuranSiswa = (function () {
    var config = {
        // default
        iuranTable: $('.iuran-table'),

        // default for modal bayar
        modalBayar: $('#modal'),
        siswaID: $('#siswa_id'),
        transactionInID: $('#transactions_in_id'),
        jenisIuranKelasID: $('#input-jenis-iuran-kelas'),
        jenisIuranKelasIDHidden: $('#jenis_iuran_kelas_id'),
        nilaiIuran: $('#input-nilai-iuran'),
        jumlahTunai: $('#input-jumlah'),
        status: $('#input-status'),
        buttonSave: $('#button-save'),

        // default bindEvents
        iuranTableMain: $('.iuran-table-main'),

        data: null
    };

    var init = function (settings) {
        config.data = settings.data;

        bindEvent();
        loadData(settings.data);
    };

    var bindEvent = function (settings) {
        config.iuranTableMain
            .on('click', '.button-bayar', function () {
                // set input tunai empty
                config.jumlahTunai.val(0);

                // get id from button
                var getSiswaID = $(this).data('siswaid');
                var getTransactionInID = $(this).data('transactioninid');
                var getJenisIuranKelasID = $(this).data('jenisiurankelasid');
                var getNilaiIuran = $(this).data('nilaiiuran');
                var getStatus = $(this).data('status');

                config.siswaID.val(getSiswaID);
                config.jenisIuranKelasID.val(getJenisIuranKelasID);
                config.jenisIuranKelasIDHidden.val(getJenisIuranKelasID);
                config.transactionInID.val(getTransactionInID);
                config.nilaiIuran.val(getNilaiIuran);
                config.status.val(getStatus);

                config.modalBayar.modal({
                    handleUpdate: true,
                    backdrop: 'static',
                    keyboard: false,
                    show: true
                });
            });

        config.jumlahTunai
            .on('keyup', function () {
                var nilai = parseInt(config.jumlahTunai.val());
                var nilaiIuran = parseInt(config.nilaiIuran.val());
                var status = config.status.val();

                console.log('nilai: ' + nilai);
                console.log('nilaiIuran: ' + nilaiIuran);

                if (status == 'LUNAS')
                {
                    if (nilai >= nilaiIuran) {

                        // config.jumlahTunai.removeClass('is-invalid');

                        config.buttonSave.prop('disabled', false);
                    }
                    else if (nilai < nilaiIuran) {

                        // config.jumlahTunai.addClass('is-invalid');

                        config.buttonSave.prop('disabled', true);
                    }
                }
            });

        config.status
            .on('change', function () {
                var nilai = parseInt(config.jumlahTunai.val());
                var nilaiIuran = parseInt(config.nilaiIuran.val());
                var status = config.status.val();

                if (status == "CICILAN")
                {
                    config.buttonSave.prop('disabled', false);
                }
                else if (status == 'LUNAS')
                {
                    if (nilai >= nilaiIuran) {

                        config.buttonSave.prop('disabled', false);
                    }
                    else if (nilai < nilaiIuran) {

                        config.buttonSave.prop('disabled', true);
                    }
                }
            });
    };

    var loadData = function () {
        var tbl = '';
        config.iuranTable.html(tbl);

        var data = config.data.siswa_iuran_info;

        if (data.length > 0)
        {
            var count = 1;

            $.each(data, function (_i, v) {
                tbl += `
                        <tr class='clickable-row' data-href="#" id="heading_` + count + `" data-toggle="collapse" data-target="#collapse_` + count + `" aria-expanded="true" aria-controls="collapse_` + count + `">
                            <td scope="row">` + v.nama_iuran + `</td>
                            <td scope="row">Rp. ` + v.nilai_iuran + `</td>
                            <td scope="row">
                            <span class="badge badge-dot mr-4">
                        `;

                    if (v.status == "LUNAS")
                    {
                        tbl += `
                            <span class="badge badge-success">` + v.status + `</span>
                        `;
                    }
                    else if (v.status == "BELUM" || v.status == "CICILAN")
                    {
                        tbl += `
                            <span class="badge badge-warning">` + v.status + `</span>
                        `;
                    }
                        tbl += `
                            </span>
                            </td>
                        `;
                    if (v.total_bayar < v.nilai_iuran)
                    {
                        tbl += `
                            <td scope="row" class="text-warning">Rp. ` + v.total_bayar + `</td>
                        `;
                    }
                    else if (v.total_bayar >= v.nilai_iuran)
                    {
                        tbl += `
                            <td scope="row" class="text-success">Rp. ` + v.total_bayar + `</td>
                        `;
                    }
                    else
                    {
                        tbl += `
                            <td scope="row" class="text-success">` + v.total_bayar + `</td>
                        `;
                    }
                        tbl += `
                            <td scope="row">` + v.tgl_bayar + `</td>
                            <td scope="row">` + v.operator + `</td>
                            <td scope="row">
                        `;
                    if (v.status == 'BELUM' || v.status == 'CICILAN')
                    {
                        tbl += `
                            <a href="#" data-toggle="modal" id="button-tambah" data-target="#modal" class="btn btn-sm btn-primary float-right button-bayar"
                            data-siswaID="` + v.siswa_id + `"
                            data-transactionInID="` + v.transaction_in_id + `"
                            data-nilaiIuran="` + v.nilai_iuran + `"
                            data-status="` + v.status + `"
                            data-jenisIuranKelasID="` + v.jenis_iuran_kelas_id + `">Bayar</a>
                        `;
                    }
                        tbl +=`
                            </td>
                            </tr>
                        `;
                if (v.iuran_info.length > 0)
                {
                    $.each(v.iuran_info, function (_i, v)
                    {
                        tbl += `
                            <tr id="collapse_` + count + `" class="collapse" aria-labelledby="heading_` + count + `" data-parent="#accordion">
                                <td></td>
                                <td></td>
                                <td>
                                    <span class="badge badge-dot mr-4">
                                        <i class="bg-warning"></i> <span class="badge badge-warning">ANGSURAN</span>
                                    </span>
                                </td>
                                <td scope="row" class="text-black">
                                    Rp. ` + v.jumlah + `
                                </td>
                                <td>
                                    ` + v.cicilan_date + `
                                </td>
                                <td>
                                    ` + v.created_by + `
                                </td>
                                <td></td>
                            </tr>
                        `;
                    });
                }

                count++;
            });
        }

        config.iuranTable.append(tbl);
    };

    return {
        config: config,
        init: init
    };

})();
//#endregion

//#region loadProfileSiswa
var loadProfileSiswa = (function () {
    var config = {
        //default
        nama: $('#nama'),

        tempatTanggalLahir: $('#tempat-tanggal-lahir'),

        kelas: $('#kelas'),
        kelasDescription: $('#kelasDescription'),

        nis: $('#nis'),
        nisDescription: $('#nisDescription'),

        jenisKelamin: $('#jenisKelamin'),
        jenisKelaminkelasDescription: $('#jenisKelaminkelasDescription'),

        agama: $('#agama'),
        agamaDescription: $('#agamaDescription'),

        alamat: $('#alamat'),
        alamatMasukDescription: $('#alamatMasukDescription'),

        noTlp: $('#noTlp'),
        noTlpDescription: $('#noTlpDescription'),

        ortuDetail: $('#ortuDetail'),
        ortuDetailMasukDescription: $('#ortuDetailMasukDescription'),

        asalSekolah: $('#asalSekolah'),
        asalSekolahDescription: $('#asalSekolahDescription'),

        tglMasuk: $('#tglMasuk'),
        tglMasukDescription: $('#tglMasukDescription'),

        data: null
    };

    var init = function (settings) {
        showLoading();

        config.data = settings.data;

        loadData();

        closeLoading();
    };

    var loadData = function () {
        config.nama.html(config.data.nama);

        config.tempatTanggalLahir.html(config.data.tempat_tgl_lahir);

        config.alamat.html(config.data.alamat);
        config.alamatMasukDescription.html("Alamat");

        config.kelas.html(config.data.kelas.name);
        config.kelasDescription.html("Kelas");

        config.nis.html(config.data.nis);
        config.nisDescription.html("NIS");

        config.jenisKelamin.html(config.data.jenis_kelamin == "L" ? 'LAKI - LAKI' : 'PEREMPUAN');
        config.jenisKelaminkelasDescription.html("Gender");

        config.agama.html(config.data.agama);
        config.agamaDescription.html("Agama");

        config.noTlp.html(config.data.no_tlp);
        config.noTlpDescription.html("No. Tlp");

        config.ortuDetail.html(config.data.nama_orang_tua + ', ' + config.data.pekerjaan_orang_tua);
        config.ortuDetailMasukDescription.html("Orang Tua");

        config.asalSekolah.html(config.data.asal_sekolah);
        config.asalSekolahDescription.html("Asal Sekolah");

        config.tglMasuk.html(config.data.tanggal_masuk);
        config.tglMasukDescription.html("Tgl Masuk");
    };

    return {
        config: config,
        init: init
    };

})();
//#endregion
