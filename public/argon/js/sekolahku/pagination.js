'use strict';

var pagination = (function () {

    // property
    var config = {
        // default
        btnSave: $('#btnSave'),
        btnSubmitAdd: $('#btnSubmitAdd'),
        textSearch: $("#text-search"),
        totalPage: $("#total_page"),
        old_text_search: $("#old_search_text"),
        text_search_element: $("#text-search"),

        // custom
        csrf_token: null,
        colName: [],
        max_row: null,
        url: null
    };

    // init
    var init = function (settings) {
        config.csrf_token = settings.csrf_token;
        config.colName = settings.colName;
        config.max_row = settings.max_row;
        config.url = settings.url;

        bindFunction();
    };

    // Bind Function
    var bindFunction = function () {
        config.btnSave.click(function () {
            config.btnSubmitAdd.click();
        });

        config.textSearch.change(function () {
            search_process();
        });

        $(document).on("click", ".data-box .card-footer .pagination li", function (e) {
            e.preventDefault();

            var curentPage = $('.data-box .card-footer .pagination li.active').text();
            var page = $.trim($(this).text());
            var liIndex = $(this).index();

            config.totalPage.val();

            console.log(page)

            if (isNaN(page)) {
                if (page === '›') {
                    page = parseInt(curentPage) + 1;

                    if (page > config.totalPage) {
                        page = parseInt(config.totalPage);
                    }
                }

                if (page === '‹') {
                    page = parseInt(curentPage) - 1;

                    if (page === 0) {
                        page = 1;
                    }
                }

                if (page === '...') {
                    page = parseInt(curentPage)
                }

                $('.data-box .card-footer .pagination li').each(function () {
                    var liPage = $(this).text();
                    var liPageIndex = $(this).index();

                    if (page == liPage) {
                        $('.data-box .card-footer .pagination li').removeClass('active');
                        $('.data-box .card-footer .pagination li').eq(liPageIndex).addClass('active');
                    }
                });
            } else {

                $('.data-box .card-footer .pagination li').removeClass('active');
                $('.data-box .card-footer .pagination li').eq(liIndex).addClass('active');
            }

            pagination_process(parseInt(page));
        });
    };

    // Search datatable
    var search_process = function () {
        search_and_pagination_api(true);
    };

    // Pagination datatable
    var pagination_process = function (page) {
        search_and_pagination_api(false, page);
    };

    // Menampilkan data datatable
    var show_data = function (dt, page, colName) {
        var tbl = '';
        page = (page * config.max_row) - config.max_row;

        if (dt.length > 0) {
            $.each(dt, function (x, y) {
                page = page + 1;
                tbl += `
                    <tr>
                        <td>`+ page + `</td>
                `;

                if (colName.length > 0) {
                    $.each(colName, function (_i, v) {
                        var concatColName = ('y.' + v);
                        var parserColName = concatColName;
                        var cName = eval(parserColName);

                        tbl += `<td>`+ cName + `</td>`;
                    });
                }

                tbl += `
                        <td class="text-right">
                            <a href="#" class="btn btn-sm btn-primary edit" data-id="` + y.id + `"><i class="fas fa-pencil-alt"></i> Edit</a>
                            <a href="#" class="btn btn-sm btn-danger delete" data-id="` + y.id + `"><i class="fas fa-trash"></i> Delete</a>
                        </td>
                    </tr>
                `;
            });

            $('.data-box tbody').html(tbl);
            $('[data-toggle="tooltip"]').tooltip();
            $('.data-box tbody td:contains(null)').html(' ');
        } else {
            alert_msg('Data tidak ditemukan!');
        }
    };

    // search kelas api
    var search_and_pagination_api = function(search_process = false, page = 0) {

        var text_search = config.text_search_element.val();

        search_process == true ? $("#text-search").val(text_search) : "";

        showLoading();

        $.ajax({
            type: 'POST',
            url: config.url,
            headers: { "X-CSRF-TOKEN": config.csrf_token },
            dataType: 'JSON',
            data: {
                'page': page,
                'text_search': search_process == true ? text_search : config.old_text_search.val(),
            },
            success: function (msg) {

                var rs = msg.rs_data;
                var dt = rs["data"];

                show_data(dt, 1, config.colName);

                if (dt.length > 0) {
                    $('.data-box .card-footer #pagination').html($(msg.pagination));
                    search_process == true ? config.old_text_search.val(text_search) : "";
                }
            },
            error: function (xhr) {
                read_error(xhr);
            },
            complete: function (_xhr) {
                closeLoading();
            }
        });
    };

    return {
        config: config,
        init: init
    };

})();
