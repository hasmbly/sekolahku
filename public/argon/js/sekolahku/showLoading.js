function showLoading() {
    $('.box_loader').fadeIn('fast');
}

function closeLoading() {
    $('.box_loader').fadeOut('fast');
}

function alert_msg(msg) {
    console.log(msg)
    var tbl = '';
    tbl += `
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    `+ msg + `
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            `;
    $('.data-box #notif-msg').html(tbl);

    $("#notif-msg").fadeTo(2000, 500).slideUp(500, function () {
        $("#notif-msg").slideUp(500);
    });
}
