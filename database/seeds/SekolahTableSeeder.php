<?php

use Illuminate\Database\Seeder;

class SekolahTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('sekolah')->insert([
            [
            'name' => 'Sekolah',
            'created_at' => now(),
            'updated_at' => now()
            ],
            [
            'name' => 'Kepala Sekolah',
            'created_at' => now(),
            'updated_at' => now()
            ],
            [
            'name' => 'Staff',
            'created_at' => now(),
            'updated_at' => now()
            ],
            [
            'name' => 'Guru',
            'created_at' => now(),
            'updated_at' => now()
            ],
            [
            'name' => 'Security',
            'created_at' => now(),
            'updated_at' => now()
            ]
        ]);
    }
}
