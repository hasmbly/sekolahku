<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([UsersTableSeeder::class]);
        $this->call([RolesTableSeeder::class]);
        $this->call([RoleUserTableSeeder::class]);
        $this->call([SekolahTableSeeder::class]);
        $this->call([KelasTableSeeder::class]);
        $this->call([JenisIuranWaktuTableSeeder::class]);
        $this->call([JenisIuranTableSeeder::class]);
    }
}
