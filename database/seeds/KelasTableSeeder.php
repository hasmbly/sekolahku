<?php

use Illuminate\Database\Seeder;

class KelasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('kelas')->insert([
            [
            'name' => 'VII',
            'created_at' => now(),
            'updated_at' => now()
            ],
            [
            'name' => 'VIII',
            'created_at' => now(),
            'updated_at' => now()
            ],
            [
            'name' => 'IX',
            'created_at' => now(),
            'updated_at' => now()
            ]
        ]);
    }
}
