<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            [
            'name' => 'ROLE_SUPERADMIN',
            'created_at' => now(),
            'updated_at' => now()
            ],
            [
            'name' => 'ROLE_ADMIN',
            'created_at' => now(),
            'updated_at' => now()
            ],
            [
            'name' => 'ROLE_USERS',
            'created_at' => now(),
            'updated_at' => now()
            ]
        ]);
    }
}
