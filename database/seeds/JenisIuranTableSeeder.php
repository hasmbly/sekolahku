<?php

use Illuminate\Database\Seeder;

class JenisIuranTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('jenis_iuran')->insert([
            [
            'name' => 'PPDB',
            'jenis_iuran_waktu_id' => 1,
            'created_at' => now(),
            'updated_at' => now()
            ],
            [
            'name' => 'UN',
            'jenis_iuran_waktu_id' => 1,
            'created_at' => now(),
            'updated_at' => now()
            ],
            [
            'name' => 'STUDY_TOUR',
            'jenis_iuran_waktu_id' => 1,
            'created_at' => now(),
            'updated_at' => now()
            ],
            [
            'name' => 'QURBAN',
            'jenis_iuran_waktu_id' => 1,
            'created_at' => now(),
            'updated_at' => now()
            ],
            [
            'name' => 'PRAMUKA',
            'jenis_iuran_waktu_id' => 1,
            'created_at' => now(),
            'updated_at' => now()
            ],
            [
            'name' => 'OSIS',
            'jenis_iuran_waktu_id' => 1,
            'created_at' => now(),
            'updated_at' => now()
            ],
            [
            'name' => 'SPP',
            'jenis_iuran_waktu_id' => 2,
            'created_at' => now(),
            'updated_at' => now()
            ],
            [
            'name' => 'LKS',
            'jenis_iuran_waktu_id' => 3,
            'created_at' => now(),
            'updated_at' => now()
            ],
            [
            'name' => 'TABUNGAN',
            'jenis_iuran_waktu_id' => 4,
            'created_at' => now(),
            'updated_at' => now()
            ],
        ]);
    }
}
