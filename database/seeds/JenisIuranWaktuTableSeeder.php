<?php

use Illuminate\Database\Seeder;

class JenisIuranWaktuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('jenis_iuran_waktu')->insert([
            [
            'name' => 'TAHUNAN',
            'created_at' => now(),
            'updated_at' => now()
            ],
            [
            'name' => 'BULANAN',
            'created_at' => now(),
            'updated_at' => now()
            ],
            [
            'name' => 'SEMESTER',
            'created_at' => now(),
            'updated_at' => now()
            ],
            [
            'name' => 'BEBAS',
            'created_at' => now(),
            'updated_at' => now()
            ]
        ]);
    }
}
