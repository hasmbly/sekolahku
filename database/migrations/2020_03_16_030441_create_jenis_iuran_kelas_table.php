<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJenisIuranKelasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jenis_iuran_kelas', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('jenis_iuran_id');
            $table->unsignedInteger('kelas_id');
            $table->integer('amount');
            $table->string('angkatan');
            $table->timestamps();

            $table->index('jenis_iuran_id');
            $table->index('kelas_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jenis_iuran_kelas');
    }
}
