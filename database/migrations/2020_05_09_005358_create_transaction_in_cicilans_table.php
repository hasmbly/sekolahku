<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionInCicilansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_in_cicilans', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('transactions_in_id');
            $table->integer('jumlah');
            $table->string('description')->nullable();
            $table->dateTime('cicilan_date');

            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_in_cicilans');
    }
}
