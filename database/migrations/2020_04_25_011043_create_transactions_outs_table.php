<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsOutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions_out', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('sekolah_id');
            $table->unsignedInteger('jenis_iuran_id');
            $table->integer('jumlah');
            $table->string('description')->nullable();
            $table->dateTime('transaction_date');

            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions_out');
    }
}
