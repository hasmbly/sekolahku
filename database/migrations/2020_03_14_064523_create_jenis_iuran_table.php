<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJenisIuranTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jenis_iuran', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('jenis_iuran_waktu_id');
            $table->string('name');
            $table->text('description')->nullable();
            $table->timestamps();

            $table->index('jenis_iuran_waktu_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jenis_iuran');
    }
}
