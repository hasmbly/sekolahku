<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSiswaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('siswa', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('kelas_id');
            $table->string('nama');
            $table->string('nis');
            $table->enum('jenis_kelamin', ['L', 'P']);
            $table->string('tempat_tgl_lahir');
            $table->string('agama');
            $table->string('alamat');
            $table->string('no_tlp');
            $table->string('nama_orang_tua');
            $table->string('pekerjaan_orang_tua');
            $table->string('asal_sekolah')->nullable();
            $table->date('tanggal_masuk');
            $table->timestamps();
            $table->softDeletes();

            $table->index('kelas_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('siswa');
    }
}
