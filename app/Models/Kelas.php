<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Kelas extends Model
{
    public $table = 'kelas';

    protected $primaryKey = 'id';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $incrementing = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    #region jenis iuran kelas
    public function jenisIuranKelas()
    {
        return $this->hasOne('App\Models\JenisIuranKelas');
    }
    #endregion

    #region kelas
    public function siswa()
    {
        return $this->hasOne('App\Models\Siswa');
    }
    #endregion

    #region siswaHistory
    public function siswaHistory()
    {
        return $this->belongsToMany('App\Models\Siswa', 'siswa_kelas_history');
    }
    #endregion

    #region jenisIuran
    public function jenisIuran()
    {
        return $this->belongsToMany('App\Models\JenisIuran', 'jenis_iuran_kelas');
    }
    #endregion
}
