<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SiswaFiles extends Model
{
    public $table = 'siswa_files';

    protected $primaryKey = 'id';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'siswa_id',
        'path',
        'type',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    #region siswa
    public function siswa()
    {
        return $this->belongsTo('App\Models\Siswa');
    }
    #endregion
}
