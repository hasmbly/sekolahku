<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Siswa extends Model
{
    public $table = 'siswa';

    protected $primaryKey = 'id';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $incrementing = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'kelas_id',
        'nama',
        'nis',
        'jenis kelamin',
        'tempat_tgl_lahir',
        'agama',
        'alamat',
        'no_tlp',
        'nama_orang_tua',
        'pekerjaan_orang_tua',
        'asal_sekolah',
        'tanggal_masuk',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    #region files
    public function files()
    {
        return $this->hasMany('App\Models\SiswaFiles');
    }
    #endregion

    #region kelasHistory
    public function kelasHistory()
    {
        return $this->belongsToMany('App\Models\kelas', 'siswa_kelas_history');
    }
    #endregion

    #region kelas
    public function kelas()
    {
        return $this->belongsTo('App\Models\Kelas');
    }
    #endregion

    #region tabungan
    public function tabungan()
    {
        return $this->hasMany('App\Models\Tabungan');
    }
    #endregion

    #region transactions
    public function transactionsIn()
    {
        return $this->belongsTo('App\Models\TransactionIn');
    }
    #endregion
}
