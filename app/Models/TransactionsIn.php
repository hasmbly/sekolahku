<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TransactionsIn extends Model
{
    public $table = 'transactions_in';

    protected $primaryKey = 'id';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $incrementing = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'siswa_id',
        'jenis_iuran_kelas_id',
        'status',
        'jumlah',
        'description',
        'transaction_date',
        'created_by',
        'updated_by'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'transaction_date' => 'datetime'
    ];

    #region Jenis Iuran Kelas
    public function jenisIuranKelas()
    {
        return $this->belongsTo('App\Models\JenisIuranKelas');
    }
    #endregion

    #region Siswa
    public function Siswa()
    {
        return $this->hasOne('App\Models\Siswa');
    }
    #endregion
}
