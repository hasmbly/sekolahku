<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JenisIuranKelas extends Model
{
    public $table = 'jenis_iuran_kelas';

    protected $primaryKey = 'id';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $incrementing = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'jenis_iuran_id',
        'kelas_id',
        'amount',
        'angkatan',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    #region jenis iuran
    public function jenisIuran()
    {
        return $this->belongsTo('App\Models\JenisIuran');
    }
    #endregion

    #region kelas
    public function kelas()
    {
        return $this->belongsTo('App\Models\Kelas');
    }
    #endregion

    #region transactions
    public function transactionsIn()
    {
        return $this->hasOne('App\Models\TransactionIn');
    }
    #endregion
}
