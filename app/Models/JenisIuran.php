<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JenisIuran extends Model
{
    public $table = 'jenis_iuran';

    protected $primaryKey = 'id';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'jenis_iuran_waktu_id',
        'name',
        'description',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    #region kelas
    public function kelas()
    {
        return $this->belongsToMany('App\Models\Kelas', 'jenis_iuran_kelas');
    }
    #endregion

    #region iuran waktu
    public function jenisIuranWaktu()
    {
        return $this->belongsTo('App\Models\JenisIuranWaktu');
    }
    #endregion

    #region jenis iuran kelas
    public function jenisIuranKelas()
    {
        return $this->hasOne('App\Models\JenisIuranKelas');
    }
    #endregion
}
