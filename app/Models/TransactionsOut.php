<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TransactionsOut extends Model
{
    public $table = 'transactions_out';

    protected $primaryKey = 'id';

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    public $incrementing = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'sekolah_id',
        'jenis_iuran_id',
        'jumlah',
        'description',
        'transaction_date',
        'created_by',
        'updated_by'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'transaction_date' => 'datetime'
    ];

    #region Jenis Iuran
    public function JenisIuranId()
    {
        return $this->belongsTo('App\Models\JenisIuran');
    }
    #endregion

    #region Siswa
    public function Siswa()
    {
        return $this->hasOne('App\Models\Sekolah');
    }
    #endregion
}
