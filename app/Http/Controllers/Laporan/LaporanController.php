<?php

namespace App\Http\Controllers\Laporan;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\TransactionsIn;
use App\Models\TransactionsOut;

class LaporanController extends Controller
{
    public function Umum(Request $request)
    {
        $no = 1; //number for auto increment
        $start_date = date('Y-m-d', strtotime($request->start_date)); //convert date to database format

        if ( $request->has('start_date') )
        {
            if ( empty($request->input('start_date')) )
            {
                return redirect()->Route('laporan/umum');
            }
            
            $data['transaction_in'] = TransactionsIn::whereDate('transaction_date', $start_date)->get();
            $data['total_in'] = $data['transaction_in']->sum('jumlah');
            $data['no'] = $no; //number
            $data['start_date'] = $request->start_date;
            
            return view('laporan.umum.index', $data);

        }else{

            $data['transaction_in'] = TransactionsIn::orderBy('created_at','DESC')->limit(10)->get();
            $data['total_in'] = $data['transaction_in']->sum('jumlah');
            $data['no'] = $no; //number
            $data['start_date'] = $request->start_date;

            return view('laporan.umum.index', $data);
        }

    }
}
