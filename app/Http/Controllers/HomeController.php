<?php

namespace App\Http\Controllers;

use App\Models\Siswa;

class HomeController extends Controller
{
    var $maxRow = 10;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $data['no'] = 1;

        $data['result'] = Siswa::paginate($this->maxRow);

        $data['siswa_count'] = Siswa::count();

        return view('dashboard', $data);
    }
}
