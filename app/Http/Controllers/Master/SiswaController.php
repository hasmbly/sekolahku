<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Siswa;
use App\Models\Kelas;
use App\Models\JenisIuran;
use App\Models\JenisIuranKelas;
use App\Models\TransactionInCicilan;
use App\Models\TransactionsIn;
use Carbon\Carbon;
use Illuminate\Support\Facades\Session;
use Validator;

class SiswaController extends Controller
{
    var $maxRow = 15;
    var $sessionMessageName = "Siswa!";

    public function index()
    {
        $data['no'] = 1;

        $data['result'] = Siswa::paginate($this->maxRow);

        $data['total_page'] = $data['result']->lastPage();

        $data['max_row'] = $this->maxRow;

        $data['list_kelas'] = Kelas::all();

        return view('master.siswa.index', $data);
    }

    public function store(Request $request)
    {
        $data = $request->all();

        $rules = [
            'kelas_id' => 'required',
            'nama' => 'required',
            'nis'  => 'required',
            'jenis_kelamin'  => 'required',
            'tempat_tgl_lahir'  => 'required',
            'agama'  => 'required',
            'alamat'  => 'required',
            'no_tlp'  => 'required',
            'nama_orang_tua'  => 'required',
            'pekerjaan_orang_tua'  => 'required',
            'asal_sekolah'  => 'required',
            'tanggal_masuk'  => 'required'
        ];

        $messages = [
            'required' => 'Gagal tambah ":attribute"! Data harus di input.'
        ];

        $attributes = [
            'kelas_id' => 'Kelas',
            'nama' => 'Nilai Bayaran',
            'nis' => 'Nis',
            'jenis_kelamin' => 'Jenis Kelamin',
            'tempat_tgl_lahir' => 'Tempat Tanggal Lahir',
            'agama' => 'Agama',
            'alamat' => 'Alamat',
            'no_tlp' => 'No. Tlp',
            'nama_orang_tua' => 'Nama Orang Tua',
            'pekerjaan_orang_tua' => 'Pekerjaan Orang Tua',
            'asal_sekolah' => 'Pekerjaan Orang Tua',
            'tanggal_masuk' => 'Tanggal Masuk'
        ];

        $validator  = Validator::make($data, $rules, $messages, $attributes);

        if ($validator->fails()) {

            $errorMsg = $validator->errors()->first();

            Session::flash('error_message', $errorMsg);

            return redirect()->back();
        }

        $siswa = new Siswa();

        $siswa->kelas_id = $request->kelas_id;
        $siswa->nama = $request->nama;
        $siswa->nis = $request->nis;
        $siswa->jenis_kelamin = $request->jenis_kelamin;
        $siswa->tempat_tgl_lahir = $request->tempat_tgl_lahir;
        $siswa->agama = $request->agama;
        $siswa->alamat = $request->alamat;
        $siswa->no_tlp = $request->no_tlp;
        $siswa->nama_orang_tua = $request->nama_orang_tua;
        $siswa->pekerjaan_orang_tua = $request->pekerjaan_orang_tua;
        $siswa->asal_sekolah = $request->asal_sekolah;
        $siswa->tanggal_masuk = date('Y-m-d', strtotime($request->tanggal_masuk));

        $siswa->save();

        Session::flash('success_message', 'Berhasil Menambah ' . $this->sessionMessageName);

        return redirect()->back();
    }

    public function show($id)
    {
        $siswa = Siswa::with('kelas')->find($id);

        $siswa['kelas'] = $siswa->kelas->name;

        // get pembayaran siswa
        $siswa['siswa_iuran_info'] = $this->pembayaran_siswa($id);

        return response()->json($siswa);
    }

    public function update(Request $request, $id)
    {
        $siswa = Siswa::find($id);

        $siswa->kelas_id = $request->kelas_id;
        $siswa->nama = $request->nama;
        $siswa->nis = $request->nis;
        $siswa->jenis_kelamin = $request->jenis_kelamin;
        $siswa->tempat_tgl_lahir = $request->tempat_tgl_lahir;
        $siswa->agama = $request->agama;
        $siswa->alamat = $request->alamat;
        $siswa->no_tlp = $request->no_tlp;
        $siswa->nama_orang_tua = $request->nama_orang_tua;
        $siswa->pekerjaan_orang_tua = $request->pekerjaan_orang_tua;
        $siswa->asal_sekolah = $request->asal_sekolah;
        $siswa->tanggal_masuk = date('Y-m-d', strtotime($request->tanggal_masuk));

        $siswa->save();

        Session::flash('success_message', 'Berhasil Merubah ' . $this->sessionMessageName);

        return redirect()->back();
    }

    public function destroy($id)
    {
        $siswa = Siswa::find($id);

        $siswa->delete();

        Session::flash('success_message', 'Berhasil Menghapus ' . $this->sessionMessageName);

        return redirect()->back();
    }

    public function SearchData(Request $req)
    {
        $rs_data = Siswa::from('siswa as a')
            ->join('kelas', 'kelas.id', '=', 'a.kelas_id')
            ->select('a.*', 'kelas.id as kelas_id', 'kelas.name as kelas');

        $textSearch = $req->text_search;
        // $kelasSearch = $req->kelas_search; // untuk filter kelas

        $rs_data->where(function ($q) use ($textSearch) {
            $q->where('a.nama', 'LIKE', '%' . $textSearch . '%');
            $q->orWhere('a.nis', 'LIKE', '%' . $textSearch . '%');
            // $q->where('kelas.name', 'LIKE', '%' . 'IX' . '%'); // untuk filter kelas
        });

        $qrData = $rs_data->paginate($this->maxRow);

        $data['rs_data'] = $qrData;
        $data['pagination'] =  (string) $qrData->links();

        return json_encode($data);
    }

    public function pembayaran_siswa($id)
    {
        $siswa = Siswa::with('kelas')->find($id);

        // get all 'jenis_iuran_kelas' base on siswa 'kelas_id' and 'angkatan'
        $list_jenisIuranKelas = JenisIuranKelas::where('kelas_id', $siswa->kelas->id)
            ->where('angkatan',  date('Y', strtotime($siswa->tanggal_masuk)))
            ->with('jenisIuran')
            ->get();

        $pembayaran = [];

        /**
         * step 1: looping for list 'jenis_iuran_kelas',
         * step 2: get 'jenis_iuran_waktu' through object relationship,
         * step 3: preparing the orray's result
         */
        if (count($list_jenisIuranKelas) > 0)
        {
            foreach ($list_jenisIuranKelas as $index => $value) {
                $dataPembayaran = [];

                $jenisIuranKelas = $list_jenisIuranKelas[$index];

                $retensiWaktu = $jenisIuranKelas->jenisIuran->jenisIuranWaktu->name;

                switch ($retensiWaktu) {
                    case 'TAHUNAN':
                        $data = [];

                        $data['nama_iuran'] = $jenisIuranKelas->jenisIuran->name;
                        $data['nilai_iuran'] = $jenisIuranKelas->amount;

                        $data['iuran_info'] = [];

                        $siswa_iuran_info = [];

                        // check if PPDB already LUNAS OR BELUM
                        if ($data['nama_iuran'] == "PPDB")
                        {
                            $siswa_iuran_info = Siswa::from('siswa as s')
                                ->select('t.id', 's.id as siswa_id', 't.jenis_iuran_kelas_id', 't.status', 't.jumlah', 't.created_at', 't.created_by')
                                ->join('transactions_in as t', 't.siswa_id', '=', 's.id')
                                ->where('t.siswa_id', $id)
                                ->where('t.jenis_iuran_kelas_id', $jenisIuranKelas->id)
                                ->get();
                        }
                        else
                        {
                            $siswa_iuran_info = Siswa::from('siswa as s')
                                ->select('t.id', 's.id as siswa_id', 't.jenis_iuran_kelas_id', 't.status', 't.jumlah', 't.created_at', 't.created_by')
                                ->join('transactions_in as t', 't.siswa_id', '=', 's.id')
                                ->where('t.siswa_id', $id)
                                ->where('t.jenis_iuran_kelas_id', $jenisIuranKelas->id)
                                ->whereYear('t.created_at', date('Y'))
                                ->get();
                        }

                        if (!empty($siswa_iuran_info) || count($siswa_iuran_info) != 0)
                        {
                            $transaction_in_id = data_get($siswa_iuran_info, '0.id');

                            $data['transaction_in_id'] = $transaction_in_id;
                            $data['jenis_iuran_kelas_id'] = $jenisIuranKelas->id;
                            $data['siswa_id'] = $id;

                            $data['status'] = data_get($siswa_iuran_info, '0.status') ? data_get($siswa_iuran_info, '0.status') : 'BELUM';
                            $data['total_bayar'] = data_get($siswa_iuran_info, '0.jumlah') ? data_get($siswa_iuran_info, '0.jumlah') : '-';
                            $data['tgl_bayar'] = data_get($siswa_iuran_info, '0.created_at') ? date('d-m-Y', strtotime(data_get($siswa_iuran_info, '0.created_at'))) : '-';
                            $data['operator'] = data_get($siswa_iuran_info, '0.created_by') ? data_get($siswa_iuran_info, '0.created_by') : '-';

                            // get iuran_info
                            // cicilan
                            $cicilan = TransactionInCicilan::from('transaction_in_cicilans as c')
                                ->select('c.*')
                                ->where('transactions_in_id', $transaction_in_id)
                                ->get();

                            if (!empty($cicilan) || count($cicilan) != 0)
                            {
                                foreach ($cicilan as $key => $value) {
                                    $cicilan[$key]->cicilan_date = date('d-m-Y', strtotime($cicilan[$key]->cicilan_date));
                                }

                                $data['iuran_info'] = $cicilan;
                            }
                        }

                        array_push($dataPembayaran, $data);

                        continue;
                    case 'BULANAN':
                        $totalMonth = [1,2,3,4,5,6,7,8,9,10,11,12];
                        $data = [];

                        foreach ($totalMonth as $key => $month) {
                            $data['nama_iuran'] = $jenisIuranKelas->jenisIuran->name;
                            $data['nilai_iuran'] = $jenisIuranKelas->amount;

                            $data['iuran_info'] = [];

                            $siswa_iuran_info = Siswa::from('siswa as s')
                                ->select('t.id', 't.status', 't.jumlah', 't.created_at', 't.created_by')
                                ->join('transactions_in as t', 't.siswa_id', '=', 's.id')
                                ->where('t.siswa_id', $id)
                                ->where('t.jenis_iuran_kelas_id', $jenisIuranKelas->id)
                                ->whereYear('t.created_at', date('Y'))
                                ->whereMonth('t.created_at', $month)
                                ->get();

                            if (!empty($siswa_iuran_info) || count($siswa_iuran_info) != 0) {
                                $transaction_in_id = data_get($siswa_iuran_info, '0.id');

                                $data['jatuh_tempo'] = $this->getMonthName($month);
                                $data['transaction_in_id'] = $transaction_in_id;
                                $data['jenis_iuran_kelas_id'] = $jenisIuranKelas->id;
                                $data['siswa_id'] = $id;

                                $data['transaction_id'] = data_get($siswa_iuran_info, '0.id');
                                $data['status'] = data_get($siswa_iuran_info, '0.status') ? data_get($siswa_iuran_info, '0.status') : 'BELUM';
                                $data['total_bayar'] = data_get($siswa_iuran_info, '0.jumlah') ? data_get($siswa_iuran_info, '0.jumlah') : '-';
                                $data['tgl_bayar'] = data_get($siswa_iuran_info, '0.created_at') ? date('d-m-Y', strtotime(data_get($siswa_iuran_info, '0.created_at'))) : '-';
                                $data['operator'] = data_get($siswa_iuran_info, '0.created_by') ? data_get($siswa_iuran_info, '0.created_by') : '-';

                                // get iuran_info
                                // cicilan
                                $cicilan = TransactionInCicilan::from('transaction_in_cicilans as c')
                                    ->select('c.*')
                                    ->where('transactions_in_id', $transaction_in_id)
                                    ->get();

                                if (!empty($cicilan) || count($cicilan) != 0) {
                                    $data['iuran_info'] = $cicilan;
                                }
                            }

                            array_push($dataPembayaran, $data);
                        }

                        continue;
                    case 'SEMESTER':
                        $data = [];

                        $data['nama_iuran'] = $jenisIuranKelas->jenisIuran->name;
                        $data['nilai_iuran'] = $jenisIuranKelas->amount;

                        $data['iuran_info'] = [];

                        $getLastDateBayaran = TransactionsIn::select('created_at')->where('siswa_id', $id)->where('jenis_iuran_kelas_id', $jenisIuranKelas->id)->get();
                        $getLastDateBayaran = data_get($siswa_iuran_info, '0.created_at');

                        $lastBayaran = Carbon::createFromFormat('Y-m-d H:i:s', $getLastDateBayaran)->format('H:i:s');
                        $nextBayaran = $lastBayaran->addMonths(6);

                        $siswa_iuran_info = Siswa::from('siswa as s')
                            ->select('t.id', 't.status', 't.jumlah', 't.created_at', 't.created_by')
                            ->join('transactions_in as t', 't.siswa_id', '=', 's.id')
                            ->where('t.siswa_id', $id)
                            ->where('t.jenis_iuran_kelas_id', $jenisIuranKelas->id)
                            ->whereBetween('created_at', [$lastBayaran, $nextBayaran])
                            ->get();

                        if (!empty($siswa_iuran_info) || count($siswa_iuran_info) != 0) {
                            $transaction_in_id = data_get($siswa_iuran_info, '0.id');

                            $data['transaction_in_id'] = $transaction_in_id;
                            $data['jenis_iuran_kelas_id'] = $jenisIuranKelas->id;
                            $data['siswa_id'] = $id;

                            $data['transaction_id'] = data_get($siswa_iuran_info, '0.id');
                            $data['status'] = data_get($siswa_iuran_info, '0.status') ? data_get($siswa_iuran_info, '0.status') : 'BELUM';
                            $data['total_bayar'] = data_get($siswa_iuran_info, '0.jumlah') ? data_get($siswa_iuran_info, '0.jumlah') : '-';
                            $data['tgl_bayar'] = data_get($siswa_iuran_info, '0.created_at') ? date('d-m-Y', strtotime(data_get($siswa_iuran_info, '0.created_at'))) : '-';
                            $data['operator'] = data_get($siswa_iuran_info, '0.created_by') ? data_get($siswa_iuran_info, '0.created_by') : '-';

                            // get iuran_info
                            // cicilan
                            $cicilan = TransactionInCicilan::from('transaction_in_cicilans as c')
                                ->select('c.*')
                                ->where('transactions_in_id', $transaction_in_id)
                                ->get();

                            if (!empty($cicilan) || count($cicilan) != 0) {
                                $data['iuran_info'] = $cicilan;
                            }
                        }

                        array_push($dataPembayaran, $data);

                        continue;
                    case 'BEBAS': // can IN or OUT ex: Tabungan
                        $data = [];

                        $data['nama_iuran'] = $jenisIuranKelas->jenisIuran->name;
                        $data['nilai_iuran'] = $jenisIuranKelas->amount;

                        $data['iuran_info'] = [];

                        $siswa_iuran_info = Siswa::from('siswa as s')
                            ->select('t.id', 't.status', 't.jumlah', 't.created_at', 't.created_by')
                            ->join('transactions_in as t', 't.siswa_id', '=', 's.id')
                            ->where('t.siswa_id', $id)
                            ->where('t.jenis_iuran_kelas_id', $jenisIuranKelas->id)
                            ->whereYear('t.created_at', date('Y'))
                            ->whereMonth('t.created_at', date('m'))
                            ->get();

                        if (!empty($siswa_iuran_info) || count($siswa_iuran_info) != 0) {
                            $transaction_in_id = data_get($siswa_iuran_info, '0.id');

                            $data['transaction_in_id'] = $transaction_in_id;
                            $data['jenis_iuran_kelas_id'] = $jenisIuranKelas->id;
                            $data['siswa_id'] = $id;

                            $data['transaction_id'] = data_get($siswa_iuran_info, '0.id');
                            $data['status'] = data_get($siswa_iuran_info, '0.status') ? data_get($siswa_iuran_info, '0.status') : 'BELUM';
                            $data['total_bayar'] = data_get($siswa_iuran_info, '0.jumlah') ? data_get($siswa_iuran_info, '0.jumlah') : '-';
                            $data['tgl_bayar'] = data_get($siswa_iuran_info, '0.created_at') ? date('d-m-Y', strtotime(data_get($siswa_iuran_info, '0.created_at'))) : '-';
                            $data['operator'] = data_get($siswa_iuran_info, '0.created_by') ? data_get($siswa_iuran_info, '0.created_by') : '-';

                            // get iuran_info
                            // Tabungan atau Iuran BEBAS yang bisa IN or OUT
                        }

                        array_push($dataPembayaran, $data);

                        continue;
                    default:
                        # code...
                        continue;
                }

                if (count($pembayaran) > 0) {

                    $dataObject = (object)
                    [
                        "jenis_waktu_iuran" => "",
                        "data_iuran" => []
                    ];

                    $isFound = false;
                    foreach ($pembayaran as $data) {

                        if ($data->jenis_waktu_iuran == $retensiWaktu) {
                            $isFound = true;
                            break;
                        }
                    }

                    if ($isFound === false) {

                        $dataObject->jenis_waktu_iuran = $retensiWaktu;
                        $dataObject->data_iuran = $dataPembayaran;

                        array_push($pembayaran, $dataObject);
                    }
                    else if ($isFound === true)
                    {
                        array_push($data->data_iuran, $dataPembayaran);
                    }
                } else
                {
                    $dataObject = (object)
                    [
                        "jenis_waktu_iuran" => "",
                        "data_iuran" => []
                    ];

                    $dataObject->jenis_waktu_iuran = $retensiWaktu;
                    $dataObject->data_iuran = $dataPembayaran;

                    array_push($pembayaran, $dataObject);
                }
            }
        }

        return $pembayaran;
    }

    public function getMonthName($value)
    {
        switch ($value) {
            case 1:
                return "Januari";
                break;
            case 2:
                return "Februari";
                break;
            case 3:
                return "Maret";
                break;
            case 4:
                return "April";
                break;
            case 5:
                return "Mei";
                break;
            case 6:
                return "Juni";
                break;
            case 7:
                return "Juli";
                break;
            case 8:
                return "Agustus";
                break;
            case 9:
                return "September";
                break;
            case 10:
                return "Oktober";
                break;
            case 11:
                return "November";
                break;
            case 12:
                return "Desember";
                break;

            default:
                return "";
                break;
        }
    }
}
