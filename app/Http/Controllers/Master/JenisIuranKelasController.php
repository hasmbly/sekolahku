<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\JenisIuran;
use App\Models\Kelas;
use App\Models\JenisIuranKelas;
use App\Models\JenisIuranWaktu;
use Illuminate\Support\Facades\Session;
use Validator;

class JenisIuranKelasController extends Controller
{

    var $maxRow = 15;
    var $sessionMessageName = "Jenis Iuran Kelas!";

    public function index()
    {
        $data['no'] = 1;

        $data['result'] = JenisIuranKelas::paginate($this->maxRow);

        // change property of jenisIuran to 'waktu'
        foreach ($data['result'] as $index => $value) {
            $data['result'][$index]->jenisIuran = $data['result'][$index]->jenisIuran->name;
        }

        // change property of kelas to 'waktu'
        foreach ($data['result'] as $index => $value) {
            $data['result'][$index]->kelas = $data['result'][$index]->kelas->name;
        }

        $data['total_page'] = $data['result']->lastPage();

        $data['max_row'] = $this->maxRow;

        $data['list_jenis_iuran'] = JenisIuran::all();

        $data['list_kelas'] = Kelas::all();

        return view('master.jenis_iuran_kelas.index', $data);
    }

    public function store(Request $request)
    {
        $data = $request->all();

        $rules = [
            'jenis_iuran_id' => 'required',
            'kelas_id' => 'required',
            'amount' => 'required',
            'angkatan'  => 'required'
        ];

        $messages = [
            'required' => 'Gagal tambah ":attribute"! Data harus di input.'
        ];

        $attributes = [
            'jenis_iuran_id' => 'Jenis Iuran',
            'kelas_id' => 'Kelas',
            'amount' => 'Nilai Bayaran',
            'angkatan' => 'Angkatan'
        ];

        $validator  = Validator::make($data, $rules, $messages, $attributes);

        if ($validator->fails()) {

            $errorMsg = $validator->errors()->first();

            Session::flash('error_message', $errorMsg);

            return redirect()->back();
        }

        $jenisIuran = JenisIuran::find($request->jenis_iuran_id);
        if ($jenisIuran->name == 'PPDB')
        {
            // validation for PPDB can't duplicate for same kelas && same angkatan
            // if exist thrown error!
            $getJenisIuranKelas = JenisIuranKelas::where('jenis_iuran_id', '=', $request->jenis_iuran_id)
                ->where('kelas_id', '=', $request->kelas_id)
                ->where('angkatan', '=', $request->angkatan)
                ->get();

            if (count($getJenisIuranKelas) > 0)
            {
                Session::flash('error_message', 'Gagal Menambah ' . $this->sessionMessageName . ' Maaf data sudah ada');

                return redirect()->back();
            }
        }

        $jenisIuranKelas = new JenisIuranKelas();

        $jenisIuranKelas->jenis_iuran_id = $request->jenis_iuran_id;
        $jenisIuranKelas->kelas_id = $request->kelas_id;
        $jenisIuranKelas->amount = $request->amount;
        $jenisIuranKelas->angkatan = $request->angkatan;

        $jenisIuranKelas->save();

        Session::flash('success_message', 'Berhasil Menambah ' . $this->sessionMessageName);

        return redirect()->back();
    }

    public function show($id)
    {
        $jenisIuranKelas = JenisIuranKelas::find($id);

        return response()->json($jenisIuranKelas);
    }

    public function update(Request $request, $id)
    {
        $jenisIuranKelas = JenisIuranKelas::find($id);

        $jenisIuranKelas->jenis_iuran_id = $request->jenis_iuran_id;
        $jenisIuranKelas->kelas_id = $request->kelas_id;
        $jenisIuranKelas->amount = $request->amount;
        $jenisIuranKelas->angkatan = $request->angkatan;

        $jenisIuranKelas->save();

        Session::flash('success_message', 'Berhasil Merubah ' . $this->sessionMessageName);

        return redirect()->back();
    }

    public function destroy($id)
    {
        $jenisIuranKelas = JenisIuranKelas::find($id);

        $jenisIuranKelas->delete();

        Session::flash('success_message', 'Berhasil Menghapus ' . $this->sessionMessageName);

        return redirect()->back();
    }

    public function SearchData(Request $req)
    {
        $rs_data = JenisIuranKelas::from('jenis_iuran_kelas as a')
            ->join('kelas', 'kelas.id', '=', 'a.kelas_id')
            ->select('a.*', 'kelas.id as kelas_id', 'kelas.name');

        $textSearch = $req->text_search;

        $rs_data->where(function ($q) use ($textSearch) {
            $q->where('kelas.name', 'LIKE', '%' . $textSearch . '%');
        });

        $qrData = $rs_data->paginate($this->maxRow);

        // change property of jenisIuran to 'waktu'
        foreach ($qrData as $index => $value) {
            $qrData[$index]->jenisIuran = $qrData[$index]->jenisIuran->name;
        }

        // change property of kelas to 'waktu'
        foreach ($qrData as $index => $value) {
            $qrData[$index]->kelas = $qrData[$index]->kelas->name;
        }

        $data['rs_data'] = $qrData;
        $data['pagination'] =  (string) $qrData->links();

        return json_encode($data);
    }
}
