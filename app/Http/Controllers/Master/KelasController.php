<?php

namespace App\Http\Controllers\Master;

use Validator;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Kelas;

class KelasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    var $maxRow = 15;

    public function index()
    {
        $data['no'] = 1;
        $data['result'] = Kelas::paginate($this->maxRow);
        // dd($data);
        $data['total_page'] = $data['result']->lastPage();
        $data['max_row'] = $this->maxRow;

        // dd($data);

        return view('master.kelas.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name'   => 'required'
        ]);

        if ($validator->fails()) {
            Session::flash('error_message', 'Gagal tambah kelas! Data harus di input');
            return redirect()->back();
        }


        $kelas = $request->name;

        $data = New Kelas();
        $data->name = $kelas;
        $data->save();

        Session::flash('success_message', 'Berhasil tambah kelas!');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $kelas = Kelas::find($id);

        return response()->json($kelas);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $kelas = Kelas::find($id);
        $kelas->name = $request->name;
        $kelas->save();

        Session::flash('success_message', 'Berhasil Merubah Kelas!');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kelas = Kelas::find($id);

        $kelas->delete();

        Session::flash('success_message', 'Berhasil Menghapus Kelas!');

        return redirect()->back();
    }

    public function SearchData(Request $req)
    {
        $rs_data = Kelas::from('kelas as a');

        $textSearch = $req->text_search;

        $rs_data->where(function($q) use ($textSearch) {
                        $q->where('a.name','LIKE', '%'.$textSearch.'%');
                        // ->orWhere('a.tahun_ajaran', 'LIKE', '%'.$textSearch.'%')
                    });

        $qrData = $rs_data->paginate($this->maxRow);

        $data['rs_data'] = $qrData;
        $data['pagination'] =  (string) $qrData->links();

        return json_encode($data);
    }
}
