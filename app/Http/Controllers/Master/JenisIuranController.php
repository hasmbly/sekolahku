<?php

namespace App\Http\Controllers\Master;

use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\JenisIuran;
use App\Models\JenisIuranWaktu;
use Illuminate\Support\Facades\Session;

class JenisIuranController extends Controller
{
    var $maxRow = 15;

    public function index()
    {
        $data['no'] = 1;

        $data['result'] = JenisIuran::paginate($this->maxRow);

        // change property of jenisIuranWaktu to 'waktu'
        foreach ( $data['result'] as $index => $value )
        {
            $data['result'][$index]->jenisIuranWaktu = $data['result'][$index]->jenisIuranWaktu->name;
        }

        $data['total_page'] = $data['result']->lastPage();

        $data['max_row'] = $this->maxRow;

        $data['list_jenis_iuran_waktu'] = JenisIuranWaktu::all();

        return view('master.jenis_iuran.index', $data);
    }

    public function store(Request $request)
    {
        $data = $request->all();

        $rules = [
            'jenis_iuran_waktu_id' => 'required',
            'name'  => 'required'
        ];

        $messages = [
            'required' => 'Gagal tambah ":attribute"! Data harus di input.'
        ];

        $attributes = [
            'jenis_iuran_waktu_id' => 'Jenis Iuran Waktu',
            'name' => 'Nama'
        ];

        $validator  = Validator::make($data, $rules, $messages, $attributes);

        if ($validator->fails()) {

            $errorMsg = $validator->errors()->first();

            Session::flash('error_message', $errorMsg);

            return redirect()->back();
        }

        $jenis_iuran_waktu_id = $request->jenis_iuran_waktu_id;
        $name = $request->name;
        $description = $request->description;

        $jenisIuran = new JenisIuran();

        $jenisIuran->jenis_iuran_waktu_id = $jenis_iuran_waktu_id;
        $jenisIuran->name = $name;
        $jenisIuran->description = $description;

        $jenisIuran->save();

        Session::flash('success_message', 'Berhasil tambah Jenis Iuran!');

        return redirect()->back();
    }

    public function show($id)
    {
        $jenisIuran = JenisIuran::find($id);

        return response()->json($jenisIuran);
    }

    public function update(Request $request, $id)
    {
        $jenisIuran = JenisIuran::find($id);
        $jenisIuran->jenis_iuran_waktu_id = $request->jenis_iuran_waktu_id;
        $jenisIuran->name = $request->name;
        $jenisIuran->description = $request->description;
        $jenisIuran->save();

        Session::flash('success_message', 'Berhasil Merubah Jenis Iuran!');

        return redirect()->back();
    }

    public function destroy($id)
    {
        $jenisIuran = JenisIuran::find($id);

        $jenisIuran->delete();

        Session::flash('success_message', 'Berhasil Menghapus Jenis Iuran!');

        return redirect()->back();
    }

    public function SearchData(Request $req)
    {
        $rs_data = JenisIuran::from('jenis_iuran as a');

        $textSearch = $req->text_search;

        $rs_data->where(function ($q) use ($textSearch) {
            $q->where('a.name', 'LIKE', '%' . $textSearch . '%');
        });

        $qrData = $rs_data->paginate($this->maxRow);

        foreach ($qrData as $index => $value) {
            $qrData[$index]->jenisIuranWaktu = $qrData[$index]->jenisIuranWaktu->name;
        }

        $data['rs_data'] = $qrData;
        $data['pagination'] =  (string) $qrData->links();

        return json_encode($data);
    }
}
