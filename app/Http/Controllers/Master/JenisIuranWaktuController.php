<?php

namespace App\Http\Controllers\Master;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\JenisIuranWaktu;
use Illuminate\Support\Facades\Session;
use Validator;

class JenisIuranWaktuController extends Controller
{
    var $maxRow = 15;
    var $sessionMessageName = "Jenis Iuran Waktu!";

    public function index()
    {
        $data['no'] = 1;

        $data['result'] = JenisIuranWaktu::paginate($this->maxRow);

        $data['total_page'] = $data['result']->lastPage();

        $data['max_row'] = $this->maxRow;

        return view('master.jenis_iuran_waktu.index', $data);
    }

    public function store(Request $request)
    {
        $data = $request->all();

        $rules = [
            'name'  => 'required'
        ];

        $messages = [
            'required' => 'Gagal tambah ":attribute"! Data harus di input.'
        ];

        $attributes = [
            'name' => 'Nama'
        ];

        $validator  = Validator::make($data, $rules, $messages, $attributes);

        if ($validator->fails()) {

            $errorMsg = $validator->errors()->first();

            Session::flash('error_message', $errorMsg);

            return redirect()->back();
        }

        $jenisIuranWaktu = new JenisIuranWaktu();

        $jenisIuranWaktu->name = $request->name;

        $jenisIuranWaktu->save();

        Session::flash('success_message', 'Berhasil Menambah ' . $this->sessionMessageName);

        return redirect()->back();
    }

    public function show($id)
    {
        $jenisIuranWaktu = JenisIuranWaktu::find($id);

        return response()->json($jenisIuranWaktu);
    }

    public function update(Request $request, $id)
    {
        $jenisIuranWaktu = JenisIuranWaktu::find($id);
        $jenisIuranWaktu->name = $request->name;
        $jenisIuranWaktu->save();

        Session::flash('success_message', 'Berhasil Merubah ' . $this->sessionMessageName);

        return redirect()->back();
    }

    public function destroy($id)
    {
        $jenisIuranWaktu = JenisIuranWaktu::find($id);

        $jenisIuranWaktu->delete();

        Session::flash('success_message', 'Berhasil Menghapus ' . $this->sessionMessageName);

        return redirect()->back();
    }

    public function SearchData(Request $req)
    {
        $rs_data = JenisIuranWaktu::from('jenis_iuran_waktu as a');

        $textSearch = $req->text_search;

        $rs_data->where(function ($q) use ($textSearch) {
            $q->where('a.name', 'LIKE', '%' . $textSearch . '%');
        });

        $qrData = $rs_data->paginate($this->maxRow);

        $data['rs_data'] = $qrData;
        $data['pagination'] =  (string) $qrData->links();

        return json_encode($data);
    }
}
