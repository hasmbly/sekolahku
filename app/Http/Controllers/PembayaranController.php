<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\JenisIuran;
use App\Models\JenisIuranKelas;
use App\Models\Siswa;
use App\Models\TransactionInCicilan;
use App\Models\TransactionsIn;
use Illuminate\Support\Facades\Session;
use Validator;

class PembayaranController extends Controller
{
    var $sessionMessageName = "Pembayaran!";

    public function index()
    {
        $data['list_jenis_iuran_kelas'] = JenisIuranKelas::all();
        $data['list_siswa'] = Siswa::all();

        return view('pembayaran.index', $data);
    }

    public function store(Request $request)
    {
        $data = $request->all();

        $rules = [
            'siswa_id' => 'required',
            'jenis_iuran_kelas_id' => 'required',
            'status'  => 'required',
            'jumlah'  => 'required'
        ];

        $messages = [
            'required' => 'Gagal tambah ":attribute"! Data harus di input.'
        ];

        $attributes = [
            'siswa_id' => 'Siswa',
            'jenis_iuran_kelas_id' => 'Jenis Iuran Kelas',
            'status' => 'Status',
            'jumlah' => 'Jumlah'
        ];

        $validator  = Validator::make($data, $rules, $messages, $attributes);

        if ($validator->fails()) {

            $errorMsg = $validator->errors()->first();

            Session::flash('error_message', $errorMsg);

            return redirect()->back();
        }

        if ($request->status == 'CICILAN')
        {
            // if transaction_in_id != null ? 'use first TrasanctionID' : 'create new'
            if (!empty($request->transactions_in_id)) // if not empty use last id of transaction_in
            {
                $TransactionInCicilan = new TransactionInCicilan();
                $TransactionInCicilan->transactions_in_id = $request->transactions_in_id;
                $TransactionInCicilan->jumlah = $request->jumlah;
                $TransactionInCicilan->description = $request->description;
                $TransactionInCicilan->created_by = $request->created_by;
                $TransactionInCicilan->cicilan_date = date('Y-m-d', strtotime($request->cicilan_date));
                $TransactionInCicilan->save();

                // get sum jumlah cicilan base on transaction_in_id
                $jumlahCicilan = TransactionInCicilan::where('transactions_in_id', $request->transactions_in_id)->sum('jumlah');

                // update jumlah (all sum cicilan)
                $TransactionsIn = TransactionsIn::with('jenisIuranKelas')->find($request->transactions_in_id);
                $TransactionsIn->jumlah = $jumlahCicilan;

                // compare with nilai iuran
                $nilaiIuran = $TransactionsIn->jenisIuranKelas->amount;

                // update transaction status (only if sumCiciclan >= nilai iuran)
                if ($jumlahCicilan >= $nilaiIuran)
                {
                    $TransactionsIn->status = "LUNAS";
                }

                $TransactionsIn->save();
            }
            else // create new transaction
            {
                $TransactionsIn = new TransactionsIn();
                $TransactionsIn->siswa_id = $request->siswa_id;
                $TransactionsIn->jenis_iuran_kelas_id = $request->jenis_iuran_kelas_id;
                $TransactionsIn->status = $request->status;
                $TransactionsIn->jumlah = $request->jumlah;
                $TransactionsIn->description = $request->description;
                $TransactionsIn->created_by = $request->created_by;
                $TransactionsIn->transaction_date = date('Y-m-d', strtotime($request->transaction_date));
                $TransactionsIn->save();

                $TransactionInCicilan = new TransactionInCicilan();
                $TransactionInCicilan->transactions_in_id = $TransactionsIn->id;
                $TransactionInCicilan->jumlah = $request->jumlah;
                $TransactionInCicilan->description = $request->description;
                $TransactionInCicilan->created_by = $request->created_by;
                $TransactionInCicilan->cicilan_date = date('Y-m-d', strtotime($request->cicilan_date));
                $TransactionInCicilan->save();
            }
        }
        else if ($request->status == 'LUNAS')
        {
            $TransactionsIn = new TransactionsIn();
            $TransactionsIn->siswa_id = $request->siswa_id;
            $TransactionsIn->jenis_iuran_kelas_id = $request->jenis_iuran_kelas_id;
            $TransactionsIn->status = $request->status;
            $TransactionsIn->jumlah = $request->jumlah;
            $TransactionsIn->description = $request->description;
            $TransactionsIn->created_by = $request->created_by;
            $TransactionsIn->transaction_date = date('Y-m-d', strtotime($request->transaction_date));
            $TransactionsIn->save();
        }

        Session::flash('success_message', 'Berhasil Menambah ' . $this->sessionMessageName);

        return redirect()->back();
    }

    public function show(Request $request, $id)
    {
        $redirect = $request->redirect;

        $siswa = Siswa::with('kelas')->find($id);

        // get all 'jenis_iuran_kelas' base on siswa 'kelas_id'
        $list_jenisIuranKelas = JenisIuranKelas::where('kelas_id', $siswa->kelas->id)->get();

        $pembayaran = [];

        /**
         * step 1: looping for list 'jenis_iuran_kelas',
         * step 2: get 'jenis_iuran_waktu' through object relationship,
         * step 3: preparing the orray's result
         */
        foreach ($list_jenisIuranKelas as $data => $value) {
            $dataPembayaran = [];
            $dataPembayaran['nama_iuran'] = $data->jenisIuran->name;
            $dataPembayaran['nilai_iuran'] = $data->amount;
            $dataPembayaran['status'] = '-';
            $dataPembayaran['total_bayar'] = '-';
            $dataPembayaran['tgl_bayar'] = '-';
            $dataPembayaran['operator'] = '-';
            $dataPembayaran['info'] = [];

            array_push($pembayaran, $dataPembayaran);

            // $retensiWaktu = $data->jenisIuran->jenisIuranWaktu->name;

            // switch ($retensiWaktu) {
            //     case 'TAHUNAN':
            //         $obejct = Siswa::fron('siswa as s')
            //             ->join()
            //         continue;
            //     case 'BULANAN':
            //         # code...
            //         continue;
            //     case 'SEMESTER':
            //         # code...
            //         continue;
            //     case 'TAHUNAN':
            //         # code...
            //         continue;
            //     case 'BEBAS':
            //         # code...
            //         continue;

            //     default:
            //         # code...
            //         break;
            // }
        }

        $data['profile'] = $siswa;

        $data['result'] = $pembayaran;

        // check jika redirect == 1 (true) atau redirect == 2 (false)
        if ($redirect == 1)
        {
            return view('transaction_in.details', $data);
        }
        else if ($redirect == 2)
        {
            return response()->json($data);
        }
    }

    public function update(Request $request, $id)
    {
    }

    public function destroy($id)
    {
    }

    public function SearchData(Request $req)
    {
    }
}
